﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Escola2
{
    public class Valida
    {
        private int cont;
        private int cont2;        

        /// <summary>
        /// metodo auxiliar ao da pesquisa para identificar se a instiução digitada existe
        /// </summary>
        /// <param name="_quaisquer"></param>
        /// <returns></returns>
        public int ValidarInstituicao(string _quaisquer)
        {

            var comando = Banco.Abrir();
            comando.CommandText = "select count(nome) from instituicao where nome = '" + _quaisquer + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont2 = dr.GetInt32(0);
            }
            return cont2;
        }

        /// <summary>
        /// valida se o nome é composto de carcteres normais
        /// </summary>
        /// <param name="_nome"></param>
        /// <returns></returns>
        public bool ValidarNome(string _nome)
        {
            if (!Regex.IsMatch(_nome, @"^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$"))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// formatação do cpf para padrão
        /// </summary>
        /// <param name="_cpf"></param>
        /// <returns></returns>
        public string CpfFormat(string _cpf)
        {
            int num = _cpf.Length - 1;
            string _cpfV = "";
            for (int i = 0; i <= num; i++)
            {
                if (!Regex.IsMatch(_cpf.Substring(i, 1), @"^[0-9]+$"))
                {
                    _cpf.Remove(i, 1);
                }
                else
                {
                    _cpfV += _cpf.Substring(i, 1);
                }
            }
            return _cpfV;
        }

        public bool ValidarCpf(string _cpf)
        {
            if (_cpf.Length <= 14 && _cpf.Length >= 11)
            {
                var _cpfV = CpfFormat(_cpf);
                int d1, d2;
                int soma = 0;
                string digitado = "";
                string calculado = "";

                // Pesos para calcular o primeiro digito
                int[] peso1 = new int[] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };

                // Pesos para calcular o segundo digito
                int[] peso2 = new int[] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
                int[] n = new int[11];
                for (int i = 0; i < 11; i++)
                {
                    n[i] = int.Parse(_cpfV.Substring(i, 1));
                }


                // Caso coloque todos os numeros iguais
                switch (_cpfV)
                {
                    case "11111111111":
                        return false;
                    case "00000000000":
                        return false;
                    case "2222222222":
                        return false;
                    case "33333333333":
                        return false;
                    case "44444444444":
                        return false;
                    case "55555555555":
                        return false;
                    case "66666666666":
                        return false;
                    case "77777777777":
                        return false;
                    case "88888888888":
                        return false;
                    case "99999999999":
                        return false;
                }


                // Calcula cada digito com seu respectivo peso
                for (int i = 0; i <= peso1.GetUpperBound(0); i++)
                    soma += (peso1[i] * Convert.ToInt32(n[i]));

                // Pega o resto da divisao
                int resto = soma % 11;

                if (resto == 1 || resto == 0)
                    d1 = 0;
                else
                    d1 = 11 - resto;

                soma = 0;

                // Calcula cada digito com seu respectivo peso
                for (int i = 0; i <= peso2.GetUpperBound(0); i++)
                    soma += (peso2[i] * Convert.ToInt32(n[i]));

                // Pega o resto da divisao
                resto = soma % 11;
                if (resto == 1 || resto == 0)
                    d2 = 0;
                else
                    d2 = 11 - resto;

                calculado = d1.ToString() + d2.ToString();
                digitado = n[9].ToString() + n[10].ToString();

                // Se os ultimos dois digitos calculados bater com
                // os dois ultimos digitos do cpf entao é válido
                if (calculado == digitado)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// metodo para padronizar o telefone
        /// </summary>
        /// <param name="_telefone"></param>
        /// <returns></returns>
        public string TelefoneFormat(string _telefone)
        {
            string telV = "";
            int num = _telefone.Length - 1;
            for (int i = 0; i <= num; i++)
            {
                if (!Regex.IsMatch(_telefone.Substring(i, 1), @"^[0-9]+$"))
                {
                    _telefone.Remove(i, 1);
                }
                else
                {
                    telV += _telefone.Substring(i, 1);
                }
            }

            return telV;
        }
       

        public bool ValidarTelefone(string _telefone)
        {
            if (_telefone.Length <= 19)
            {
                _telefone = TelefoneFormat(_telefone);
                if (_telefone.Length == 9 || _telefone.Length == 11 || _telefone.Length == 13)
                {
                    for (int i = 0; i < _telefone.Length; i++)
                    {
                        if (Regex.IsMatch(_telefone.Substring(i, 1), @"^[0-9]+$"))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            return false;
        }       

        /// <summary>
        /// metodo que deixa todos rgs com um formato padrão
        /// </summary>
        /// <param name="_rg"></param>
        /// <returns></returns>
        public string RgFormat(string _rg)
        {
            int num = _rg.Length - 1;
            string _rgV = "";
            for (int i = 0; i <= num; i++)
            {
                if (!Regex.IsMatch(_rg.Substring(i, 1), @"^[0-9]+$"))
                {
                    _rg.Remove(i, 1);
                }
                else
                {
                    _rgV += _rg.Substring(i, 1);
                }
            }
            return _rgV;
        }

        /// <summary>
        /// metodo para validação do rg
        /// </summary>
        /// <param name="_rg"></param>
        /// <returns></returns>
        public bool ValidarRG(string _rg)
        {
            var _rgV = RgFormat(_rg);
            if (_rgV.Length < 10)
            {
                int[] tamanho = new int[9];
                tamanho[0] = int.Parse(_rgV.Substring(0, 1)) * 2;
                tamanho[1] = int.Parse(_rgV.Substring(1, 1)) * 3;
                tamanho[2] = int.Parse(_rgV.Substring(2, 1)) * 4;
                tamanho[3] = int.Parse(_rgV.Substring(3, 1)) * 5;
                tamanho[4] = int.Parse(_rgV.Substring(4, 1)) * 6;
                tamanho[5] = int.Parse(_rgV.Substring(5, 1)) * 7;
                tamanho[6] = int.Parse(_rgV.Substring(6, 1)) * 8;
                tamanho[7] = int.Parse(_rgV.Substring(7, 1)) * 9;
                tamanho[8] = int.Parse(_rgV.Substring(8, 1)) * 100;
                var total = 0;
                for (int i = 0; i < 9; i++)
                {
                    total += tamanho[i];
                }
                var resto = 0;
                resto = total % 11;
                if (resto != 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// metodo para validar o dia 
        /// </summary>
        /// <param name="_dia"></param>
        /// <returns></returns>
        private bool ValidarDia(string _dia)
        {
            if (_dia.Length <= 2 && _dia.Length != 0)
            {
                for (int i = 0; i < _dia.Length; i++)
                {
                    if (!Regex.IsMatch(_dia.Substring(i, 1), @"^[0-9]+$"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Metodo para validar o ano
        /// </summary>
        /// <param name="_ano"></param>
        /// <returns></returns>
        private bool ValidarAno(string _ano)
        {
            if (_ano.Length >= 2 && _ano.Length <= 4)
            {
                for (int i = 0; i < _ano.Length; i++)
                {
                    if (!Regex.IsMatch(_ano.Substring(i, 1), @"^[0-9]+$"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// metoedo para validar o mes
        /// </summary>
        /// <param name="_mes"></param>
        /// <returns></returns>
        private bool ValidarMes(string _mes)
        {
            if (_mes.Length <= 2 && _mes.Length != 0)
            {
                for (int i = 0; i < _mes.Length; i++)
                {
                    if (!Regex.IsMatch(_mes.Substring(i, 1), @"^[0-9]+$"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public string NascimentoFormat(string _dia, string _mes, string _ano)
        {
            string nascimento = "";


            if (ValidarDia(_dia))
            {
                if (_dia.Length == 1)
                {
                    _dia.Insert(0, "0");
                    nascimento = _dia;
                }
                else
                    nascimento = _dia;
            }
            if (ValidarMes(_mes))
            {
                if (_mes.Length == 1)
                {
                    _mes.Insert(0, "0");
                    nascimento += _mes;
                }
                else
                {
                    nascimento += _mes;
                }
            }
            if (ValidarAno(_ano))
            {
                nascimento += _ano;
            }
            nascimento.Insert(2, "/");
            nascimento.Insert(5, "/");
            return nascimento;
        }

        /// <summary>
        /// Metodo para validar a data de nascimento
        /// </summary>
        /// <param name="_nascimento"></param>
        /// <returns></returns>
        public bool ValidarNascimento(string _nascimento)
        {
            if (_nascimento.Length == 10 || _nascimento.Length == 8)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ValidarPai(int _id)
        {
            var comando = Banco.Abrir();
            comando.CommandText = "select count(id) from responsavel where id = '" + _id + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont = dr.GetInt32(0);
            }
            if (cont == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
