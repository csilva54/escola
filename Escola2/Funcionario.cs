﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Data;

namespace Escola2
{
    public class Funcionario
    {
        private int cont;
        private int cont2;
        private int cont3;
        private int id;
        private string nome;
        private string rg;
        private string cpf;
        private string diaNascimento;
        private string mesNascimento;
        private string anoNascimento;
        private string nascimento;
        private string telefone_funcionario;
        private bool ativo;
        private int cod_nivel;
        private int cod_instituicao;
        private int cod_cargo;
        Valida valida = new Valida();

        public int Id { get => id; set => id = value; }
        public string Nome { get => nome; set => nome = value; }
        public string Rg { get => rg; set => rg = value; }
        public string Cpf { get => cpf; set => cpf = value; }
        public string Nascimento { get => nascimento; set => nascimento = value; }
        public string Telefone_funcionario { get => telefone_funcionario; set => telefone_funcionario = value; }
        public bool Ativo { get => ativo; set => ativo = value; }
        public int Cod_nivel { get => cod_nivel; set => cod_nivel = value; }
        public int Cod_instituicao { get => cod_instituicao; set => cod_instituicao = value; }
        public string DiaNascimento { get => diaNascimento; set => diaNascimento = value; }
        public string MesNascimento { get => mesNascimento; set => mesNascimento = value; }
        public string AnoNascimento { get => anoNascimento; set => anoNascimento = value; }
        public int Cod_cargo { get => cod_cargo; set => cod_cargo = value; }

        /// <summary>
        /// metodos construtores:
        /// 1.metodo vazio
        /// 2.metodo com id
        /// 3.metodo com cpf e id
        /// 4.metodo com cpf
        /// 5.metodo com todos os campos
        /// 6.metodo com todos os campos exceto id
        /// 7.metodo com todos os campos menos chaves estrangeiras e ativo
        /// 8.metodo com todos os campos menos chaves estrangeiras, id e ativo
        /// </summary>

        public Funcionario() { }
        public Funcionario(int _id) { this.id = _id; }
        public Funcionario(int _id, string _cpf) { this.id = _id; this.cpf = _cpf; }
        public Funcionario(string _cpf) { this.cpf = _cpf; }
        public Funcionario(int _id, string _nome, string _rg, string _cpf, string _nascimento, string _telefone_funcionario, bool _ativo, int _cod_nivel, int _cod_instituicao, int _cod_cargo)
        {
            this.id = _id;
            this.nome = _nome;
            this.rg = _rg;
            this.cpf = _cpf;
            this.nascimento = _nascimento;
            this.telefone_funcionario = _telefone_funcionario;
            this.ativo = _ativo;
            this.cod_nivel = _cod_nivel;
            this.cod_instituicao = _cod_instituicao;
            this.cod_cargo = _cod_cargo;
        }
        public Funcionario(string _nome, string _rg, string _cpf, string _nascimento, string _telefone_funcionario, bool _ativo, int _cod_nivel, int _cod_instituicao, int _cod_cargo)
        {
            this.nome = _nome;
            this.rg = _rg;
            this.cpf = _cpf;
            this.nascimento = _nascimento;
            this.telefone_funcionario = _telefone_funcionario;
            this.ativo = _ativo;
            this.cod_nivel = _cod_nivel;
            this.cod_instituicao = _cod_instituicao;
            this.cod_cargo = _cod_cargo;
        }
        public Funcionario(int _id, string _nome, string _rg, string _cpf, string _nascimento, string _telefone_funcionario)
        {
            this.id = _id;
            this.nome = _nome;
            this.rg = _rg;
            this.cpf = _cpf;
            this.nascimento = _nascimento;
            this.telefone_funcionario = _telefone_funcionario;
        }
        public Funcionario(string _nome, string _rg, string _cpf, string _nascimento, string _telefone_funcionario)
        {
            this.nome = _nome;
            this.rg = _rg;
            this.cpf = _cpf;
            this.nascimento = _nascimento;
            this.telefone_funcionario = _telefone_funcionario;
        }
        /// <summary>
        /// metodo para inserir um funcionario
        /// </summary>
        public void Inserir()
        {
            bool rgV = false;
            bool nomeV = false;
            bool telefoneV = false;
            bool cpfV = false;
            bool nascimentoV = false;
            try
            {
                var comm = Banco.Abrir();

                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.CommandText = "sp_funcionario_inserir";


                if (nome != string.Empty)
                {
                    if (valida.ValidarNome(nome))
                    {
                        comm.Parameters.Add("spnome", MySqlDbType.VarChar).Value = nome;
                        nomeV = true;
                    }
                }

                if (rg != string.Empty && rg.Length <= 12)
                {
                    if (valida.ValidarRG(rg))
                    {
                        rg = valida.RgFormat(rg);
                        if (RgExistente(rg) == 0)
                        {
                            comm.Parameters.Add("sprg", MySqlDbType.VarChar).Value = rg;
                            rgV = true;
                        }
                    }
                }
                if (telefone_funcionario != string.Empty)
                {
                    if (valida.ValidarTelefone(telefone_funcionario))
                    {
                        telefone_funcionario = valida.TelefoneFormat(telefone_funcionario);
                        if (TelefoneExiste(telefone_funcionario) == 0)
                        {
                            comm.Parameters.Add("sptelefone", MySqlDbType.VarChar).Value = telefone_funcionario;
                            telefoneV = true;
                        }
                    }
                }
                if (cpf != string.Empty)
                {
                    if (valida.ValidarCpf(cpf))
                    {
                        cpf = valida.CpfFormat(cpf);
                        if (CpfExistente(cpf) == 0)
                        {
                            comm.Parameters.Add("spcpf", MySqlDbType.VarChar).Value = cpf;
                            cpfV = true;
                        }
                    }
                }
                nascimento = valida.NascimentoFormat(diaNascimento, mesNascimento, anoNascimento);
                if (valida.ValidarNascimento(nascimento))
                {
                    comm.Parameters.Add("spnascimento", MySqlDbType.VarChar).Value = nascimento;
                    nascimentoV = true;
                }
                else
                {
                    nascimentoV = false;
                }
                comm.Parameters.Add("spativo", MySqlDbType.Bit).Value = ativo;
                comm.Parameters.Add("spcod_nivel", MySqlDbType.Int32).Value = cod_nivel;
                if (cod_instituicao.ToString() != string.Empty)
                {
                    comm.Parameters.Add("spinstituicao", MySqlDbType.Int32).Value = cod_instituicao;
                }
                if (rgV &&
                     nomeV &&
                     telefoneV &&
                     cpfV &&
                     nascimentoV)
                {
                    MessageBox.Show("Funcionário inserido com sucesso");
                }
                id = Convert.ToInt32(comm.ExecuteScalar());
                comm.Connection.Close();
            }
            catch
            {
                if (rgV == false)
                    MessageBox.Show("Rg inválido");
                else if (nomeV == false)
                    MessageBox.Show("Nome inválido");
                else if (telefoneV == false)
                    MessageBox.Show("Telefone inválido");
                else if (cpfV == false)
                    MessageBox.Show("Cpf inválido");
                else if (nascimentoV == false)
                    MessageBox.Show("Data de nascimendo inválida");
            }
        }
        
        /// <summary>
        /// metodo que identifica se o rg ja existe no banco
        /// </summary>
        /// <param name="_rg"></param>
        /// <returns></returns>
        private int RgExistente(string _rg)
        {

            var comando = Banco.Abrir();
            comando.CommandText = "select count(rg) from funcionario where rg = '" + _rg + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont = dr.GetInt32(0);
            }
            return cont;
        }
     
        /// <summary>
        /// metodo que confere se o cpf ja existe
        /// </summary>
        /// <param name="_cpf"></param>
        /// <returns></returns>
        private int CpfExistente(string _cpf)
        {
            var comando = Banco.Abrir();
            comando.CommandText = "select count(cpf) from funcionario where cpf = '" + _cpf + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont2 = dr.GetInt32(0);
            }
            return cont2;
        }
        
        /// <summary>
        /// metodo para ver se o telefone n existe
        /// </summary>
        /// <param name="_telefone"></param>
        /// <returns></returns>
        private int TelefoneExiste(string _telefone)
        {
            var comando = Banco.Abrir();
            comando.CommandText = "select count(telefone) from funcionario where telefone = '" + _telefone + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont3 = dr.GetInt32(0);
            }
            return cont3;

        }
        /// <summary>
        /// metodo para alterar informações do funcionario
        /// </summary>
        public void Alterar()
        {
            bool rgV = false;
            bool nomeV = false;
            bool telefoneV = false;
            bool cpfV = false;
            bool nascimentoV = false;
            try
            {
                var comm = Banco.Abrir();

                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.CommandText = "sp_funcionario_update";


                if (nome != string.Empty)
                {
                    if (valida.ValidarNome(nome))
                    {
                        comm.Parameters.Add("spnome", MySqlDbType.VarChar).Value = nome;
                        nomeV = true;
                    }
                }

                if (rg != string.Empty && rg.Length <= 12)
                {
                    if (valida.ValidarRG(rg))
                    {
                        rg = valida.RgFormat(rg);
                        if (RgExistente(rg) == 0)
                        {
                            comm.Parameters.Add("sprg", MySqlDbType.VarChar).Value = rg;
                            rgV = true;
                        }
                    }
                }
                if (telefone_funcionario != string.Empty)
                {
                    if (valida.ValidarTelefone(telefone_funcionario))
                    {
                        telefone_funcionario = valida.TelefoneFormat(telefone_funcionario);
                        if (TelefoneExiste(telefone_funcionario) == 0)
                        {
                            comm.Parameters.Add("sptelefone", MySqlDbType.VarChar).Value = telefone_funcionario;
                            telefoneV = true;
                        }
                    }
                }
                if (cpf != string.Empty)
                {
                    if (valida.ValidarCpf(cpf))
                    {
                        cpf = valida.CpfFormat(cpf);
                        if (CpfExistente(cpf) == 0)
                        {
                            comm.Parameters.Add("spcpf", MySqlDbType.VarChar).Value = cpf;
                            cpfV = true;
                        }
                    }
                }
                nascimento = valida.NascimentoFormat(diaNascimento, mesNascimento, anoNascimento);
                if (valida.ValidarNascimento(nascimento))
                {
                    comm.Parameters.Add("spnascimento", MySqlDbType.VarChar).Value = nascimento;
                    nascimentoV = true;
                }
                else
                {
                    nascimentoV = false;
                }
                comm.Parameters.Add("spativo", MySqlDbType.Bit).Value = ativo;
                comm.Parameters.Add("spcod_nivel", MySqlDbType.Int32).Value = cod_nivel;
                comm.Parameters.Add("spcod_cargo", MySqlDbType.Int32).Value = cod_cargo;
                if (cod_instituicao.ToString() != string.Empty)
                {
                    comm.Parameters.Add("spinstituicao", MySqlDbType.Int32).Value = cod_instituicao;
                }
                if (rgV &&
                     nomeV &&
                     telefoneV &&
                     cpfV &&
                     nascimentoV)
                {
                    MessageBox.Show("Funcionário alterado com sucesso");
                }
                id = Convert.ToInt32(comm.ExecuteScalar());
                comm.Connection.Close();
            }
            catch
            {
                if (rgV == false)
                    MessageBox.Show("Rg inválido");
                else if (nomeV == false)
                    MessageBox.Show("Nome inválido");
                else if (telefoneV == false)
                    MessageBox.Show("Telefone inválido");
                else if (cpfV == false)
                    MessageBox.Show("Cpf inválido");
                else if (nascimentoV == false)
                    MessageBox.Show("Data de nascimendo inválida");
            }
        }
        /// <summary>
        /// metodo para consulta co  id
        /// </summary>
        /// <param name="_id"></param>
        public void ConsultarPorId(int _id)
        { 
            var comm = Banco.Abrir();
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "sp_funcionario_select_id";
            comm.Parameters.Add("spid", MySqlDbType.Int32).Value = _id;
            var dr = comm.ExecuteReader();
            while (dr.Read())
            {
                this.id = dr.GetInt32(0);
                this.nome = dr.GetString(1);
                this.rg = dr.GetString(2);
                this.cpf = dr.GetString(2);
                this.nascimento = dr.GetString(4);
                this.telefone_funcionario = dr.GetString(5);
                this.cod_instituicao = dr.GetInt32(8);
                this.cod_cargo = dr.GetInt32(11);
            }
            comm.Connection.Close();
        }
        /// <summary>
        /// metodo para consulta
        /// </summary>
        /// <returns></returns>
        public MySqlDataReader ConsultarTodos()
        {
            var comm = Banco.Abrir();
            comm.CommandText = "select * from funcionario";
            var dr = comm.ExecuteReader();
            return dr;
        }
                    
        public bool Conferir(string cpf)
        {
            if (valida.ValidarCpf(cpf))
            {
                cpf = valida.CpfFormat(cpf);
                if (CpfExistente(cpf) == 1)
                {
                    return false;
                }
            }
            return true;
        }

        public int ConsultarId(string cpf) // declarar método
        { //implementar método
            var comm = Banco.Abrir();
            comm.CommandText = "select id from funcionario where cpf = '" + cpf + "'";
            var dr = comm.ExecuteReader();
            while (dr.Read())
            {
                this.id = dr.GetInt32(0);
            }
            return id;
        }
        public DataTable ListarGrid()
        {
            var comm = Banco.Abrir();
            string strComm = "select f.id as ID, f.nome as Nome,f.telefone as Telefone,f.cpf as CPF,f.rg as RG,f.nascimento as Nascimento,i.nome as Instituicao,n.nivel as Nivel, c.nome as Cargo from Funcionario as f inner join instituicao as i on f.cod_instituicao = i.id inner join nivel as n on f.cod_nivel = n.id inner join cargo as c onf.cod_cargo = c.id where ativo = true;";
            string strCon = @"server=127.0.0.1;user id=root;database=escola";
            MySqlConnection cn = new MySqlConnection(strCon);
            MySqlCommand objCommand = new MySqlCommand(strComm, cn);
            MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
            DataTable dt = new DataTable();
            objAdapter.Fill(dt);
            return dt;
        }
    }
}
