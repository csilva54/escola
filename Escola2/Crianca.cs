﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Data;

namespace Escola2
{
    public class Crianca
    {
        private int cont;
        private int id;
        private string nome;
        private string diaNascimento;
        private string mesNascimento;
        private string anoNascimento;
        private string data_nascimento;
        private int id_responsavel;
        private bool ativo;
        string strComm = "";
        Valida valida = new Valida(); 

        public int Id { get => id; set => id = value; }
        public string Nome { get => nome; set => nome = value; }
        public string Data_nascimento { get => data_nascimento; set => data_nascimento = value; }
        public int Id_responsavel { get => id_responsavel; set => id_responsavel = value; }
        public bool Ativo { get => ativo; set => ativo = value; }
        public string DiaNascimento { get => diaNascimento; set => diaNascimento = value; }

        internal static bool Conferir(string text)
        {
            throw new NotImplementedException();
        }

        public string MesNascimento { get => mesNascimento; set => mesNascimento = value; }
        public string AnoNascimento { get => anoNascimento; set => anoNascimento = value; }

        public Crianca() { }
        public Crianca(int _id, int _id_responsavel) { this.id = _id; this.id_responsavel = _id_responsavel; }
        public Crianca(string _nome, int _id_responsavel) { this.nome = _nome; this.id_responsavel = _id_responsavel; }
        public Crianca(int _id, string _nome, string _data_nascimeto, int _id_responsavel, bool _ativo)
        {
            this.id = _id;
            this.nome = _nome;
            this.data_nascimento = _data_nascimeto;
            this.id_responsavel = _id_responsavel;
            this.ativo = _ativo;
        }
        public Crianca(string _nome, string _data_nascimeto, int _id_responsavel, bool _ativo)
        {
            this.nome = _nome;
            this.data_nascimento = _data_nascimeto;
            this.id_responsavel = _id_responsavel;
            this.ativo = _ativo;
        }
        public void Inserir()
        {
            bool nomeV = false;
            bool nascimentoV = false;
            bool paiV = false;
            try
            {
                var comm = Banco.Abrir();

                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.CommandText = "sp_crianca_inserir";

                if (nome != string.Empty)
                {
                    if (valida.ValidarNome(nome))
                    {
                        comm.Parameters.Add("spnome", MySqlDbType.VarChar).Value = nome;
                        nomeV = true;
                    }
                }
                data_nascimento = valida.NascimentoFormat(diaNascimento, mesNascimento, anoNascimento);
                if (valida.ValidarNascimento(data_nascimento))
                {
                    comm.Parameters.Add("spnascimento", MySqlDbType.VarChar).Value = data_nascimento;
                    nascimentoV = true;
                }
                if (id_responsavel.ToString() != string.Empty && valida.ValidarPai(id_responsavel))
                {
                    comm.Parameters.Add("spid_responsavel", MySqlDbType.Int32).Value = id_responsavel;
                    paiV = true;
                }
                if (nomeV && nascimentoV && paiV)
                {
                    MessageBox.Show("Criança inserida com sucesso");
                }

                id = Convert.ToInt32(comm.ExecuteScalar());
                comm.Connection.Close();
            }
            catch
            {
                if (nomeV == false)
                {
                    MessageBox.Show("Nome inválido");
                }
                else if (nascimentoV == false)
                {
                    MessageBox.Show("Nascimento inválido");
                }
                else if (paiV == false)
                {
                    MessageBox.Show("Pai inválido");
                }
            }

        }       
                                     
        public void Alterar()
        {
            bool nomeV = false;
            bool nascimentoV = false;
            bool paiV = false;
            try
            {
                var comm = Banco.Abrir();

                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.CommandText = "sp_crianca_update";

                if (nome != string.Empty)
                {
                    if (valida.ValidarNome(nome))
                    {
                        comm.Parameters.Add("spnome", MySqlDbType.VarChar).Value = Nome;
                        nomeV = true;
                    }
                }
                data_nascimento = valida.NascimentoFormat(diaNascimento, mesNascimento, anoNascimento);
                if (valida.ValidarNascimento(data_nascimento))
                {
                    comm.Parameters.Add("spnascimento", MySqlDbType.VarChar).Value = Data_nascimento;
                    nascimentoV = true;
                }
                if (id_responsavel.ToString() != string.Empty && valida.ValidarPai(id_responsavel))
                {
                    comm.Parameters.Add("spid_responsavel", MySqlDbType.Int32).Value = Id_responsavel;
                    paiV = true;
                }
                if (nomeV && nascimentoV && paiV)
                {
                    MessageBox.Show("Criança inserida com sucesso");
                }

                id = Convert.ToInt32(comm.ExecuteScalar());
                comm.Connection.Close();
            }
            catch
            {
                if (nomeV == false)
                {
                    MessageBox.Show("Nome inválido");
                }
                else if (nascimentoV == false)
                {
                    MessageBox.Show("Nascimento inválido");
                }
                else if (paiV == false)
                {
                    MessageBox.Show("Pai inválido");
                }
            }

        }
        public void ConsultarPorId(int _id) // declarar método
        { //implementar método
            var comm = Banco.Abrir();
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "sp_crianca_select_id";
            comm.Parameters.Add("spid", MySqlDbType.Int32).Value = _id;
            var dr = comm.ExecuteReader();
            while (dr.Read())
            {
                this.id = dr.GetInt32(0);
                this.nome = dr.GetString(1);
                this.data_nascimento = dr.GetString(2);
                this.id_responsavel = dr.GetInt32(3);

            }
            comm.Connection.Close();
        }
        /// <summary>
        /// metodo para consultar todos responsaveis
        /// </summary>
        /// <returns></returns>
        public MySqlDataReader ConsultarTodos()
        {
            var comm = Banco.Abrir();
            comm.CommandText = "select * from crianca";
            var dr = comm.ExecuteReader();
            return dr;
        }

        public DataTable ListarGrid()
        {
            var comm = Banco.Abrir();
            string strComm = "select c.Id as Codigo, c.nome as Nome, c.data_nascimento as 'Data de Nascimento', r.nome as Reponsavel,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id inner join Crianca as c on c.id_responsavel = r.id where r.ativo = true and c.ativo = true;";
            string strCon = @"server=127.0.0.1;user id=root;database=escola";
            MySqlConnection cn = new MySqlConnection(strCon);
            MySqlCommand objCommand = new MySqlCommand(strComm, cn);
            MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
            DataTable dt = new DataTable();
            objAdapter.Fill(dt);
            return dt;
        }

        public DataTable Pesquisa(string quaisquer)
        {

            var comm = Banco.Abrir();
            try
            {
                if (valida.ValidarNome(quaisquer) && valida.ValidarInstituicao(quaisquer) == 0)
                    strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where r.nome = '" + quaisquer + "' and r.ativo = true;";
                else if (valida.ValidarCpf(quaisquer))
                    strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where r.cpf = '" + quaisquer + "' and r.ativo = true;";
                else if (valida.ValidarTelefone(quaisquer))
                    strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where r.telefone = '" + quaisquer + "' and r.ativo = true;";
                else if (valida.ValidarInstituicao(quaisquer) != 0)
                    strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where inst.nome = '" + quaisquer + "' and r.ativo = true;";
                string strCon = @"server=127.0.0.1;user id=root;database=escola";
                MySqlConnection cn = new MySqlConnection(strCon);
                MySqlCommand objCommand = new MySqlCommand(strComm, cn);
                MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
                DataTable dt = new DataTable();
                objAdapter.Fill(dt);
                return dt;
            }
            catch
            {
                strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where r.nome = 'limpo';";
                string strCon = @"server=127.0.0.1;user id=root;database=escola";
                MySqlConnection cn = new MySqlConnection(strCon);
                MySqlCommand objCommand = new MySqlCommand(strComm, cn);
                MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
                DataTable dt = new DataTable();
                objAdapter.Fill(dt);
                return dt;
            };
        }

    }
}
