﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Data;


namespace Escola2
{
    public class Responsave
    {
        //obs: Nome da classe ta "Responsave" pq bugou aqui e tive q tirar o "L"
        private int cont;
        private int cont2;
        private int cont3;
        private int id;
        private string rg;
        private string nome;
        private string login;
        private string senha;
        private string telefone;
        private string cpf;
        private bool ativo;
        private int cod_nivel;
        private int cod_instituicao;
        Valida valida = new Valida();

        public int Id { get => id; set => id = value; }
        public string Rg { get => rg; set => rg = value; }
        public string Nome { get => nome; set => nome = value; }
        public string Login { get => login; set => login = value; }
        public string Senha { get => senha; set => senha = value; }
        public string Telefone { get => telefone; set => telefone = value; }
        public string Cpf { get => cpf; set => cpf = value; }
        public bool Ativo { get => ativo; set => ativo = value; }
        public int Cod_nivel { get => cod_nivel; set => cod_nivel = value; }
        public int Cod_instituicao { get => cod_instituicao; set => cod_instituicao = value; }
        /// <summary>
        /// Metodos construrores:
        /// 1.metodo vazio
        /// 2.metodo somente com id
        /// 3.metodo com id e cpf
        /// 4.metodo com cpf
        /// 5.metodo com todos os valores exceto chaves estrangeiras e ativo
        /// 6.metodo com todos os valores exceto chaves estrangeiras, ativo e id
        /// 7.metodo com todos valores
        /// 8.metodo com todos valores exceto id
        /// </summary>
        public Responsave() { }
        public Responsave(int _id) { this.id = _id; }
        public Responsave(int _id, string _cpf) { this.id = _id; this.cpf = _cpf; }
        public Responsave(string _cpf) { this.cpf = _cpf; }
        public Responsave(int _id, string _rg, string _nome, string _login, string _senha, string _telefone, string _cpf)
        {
            this.id = _id;
            this.rg = _rg;
            this.nome = _nome;
            this.login = _login;
            this.senha = _senha;
            this.telefone = _telefone;
            this.cpf = _cpf;
        }
        public Responsave(string _rg, string _nome, string _login, string _senha, string _telefone, string _cpf)
        {
            this.rg = _rg;
            this.nome = _nome;
            this.login = _login;
            this.senha = _senha;
            this.telefone = _telefone;
            this.cpf = _cpf;

        }
        public Responsave(int _id, string _rg, string _nome, string _login, string _senha, string _telefone, string _cpf, bool _ativo, int _cod_nivel, int _cod_instituicao)
        {
            this.id = _id;
            this.rg = _rg;
            this.nome = _nome;
            this.login = _login;
            this.senha = _senha;
            this.telefone = _telefone;
            this.cpf = _cpf;
            this.ativo = _ativo;
            this.cod_nivel = _cod_nivel;
            this.cod_instituicao = _cod_instituicao;
        }
        public Responsave(string _rg, string _nome, string _login, string _senha, string _telefone, string _cpf, bool _ativo, int _cod_nivel, int _cod_instituicao)
        {
            this.rg = _rg;
            this.nome = _nome;
            this.login = _login;
            this.senha = _senha;
            this.telefone = _telefone;
            this.cpf = _cpf;
            this.ativo = _ativo;
            this.cod_nivel = _cod_nivel;
            this.cod_instituicao = _cod_instituicao;
        }
        /// <summary>
        /// metodo para inserir usuario:
        /// os "ifs" sao para validações de campos vazios, de entrada de dados e de existencia de valor
        /// </summary>
        public void Inserir()
        {
            bool rgV = false;
            bool nomeV = false;
            bool telefoneV = false;
            bool cpfV = false;            

            try
            {
                var comm = Banco.Abrir();

                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.CommandText = "sp_responsavel_inserir";
                
                if (rg != string.Empty && rg.Length <= 12)
                {
                    if (valida.ValidarRG(rg))
                    {
                        rg = valida.RgFormat(rg);
                        if (RgExistente(rg) == 0)
                        {
                            comm.Parameters.Add("sprg", MySqlDbType.VarChar).Value = rg;
                            rgV = true;
                        }
                    }
                }
                if (nome != string.Empty)
                {
                    if (valida.ValidarNome(nome))
                    {
                        comm.Parameters.Add("spnome", MySqlDbType.VarChar).Value = nome;
                        nomeV = true;
                    }
                }
                if (telefone != string.Empty)
                {
                    if (valida.ValidarTelefone(telefone))
                    {
                        telefone = valida.TelefoneFormat(telefone);
                        if (TelefoneExiste(telefone) == 0)
                        {
                            comm.Parameters.Add("sptelefone", MySqlDbType.VarChar).Value = telefone;
                            telefoneV = true;
                        }
                    }
                }
                if (cpf != string.Empty)
                {
                    if (valida.ValidarCpf(cpf))
                    {
                        cpf = valida.CpfFormat(cpf);
                        if (CpfExistente(cpf) == 0)
                        {
                            comm.Parameters.Add("spcpf", MySqlDbType.VarChar).Value = cpf;
                            cpfV = true;
                        }
                    }
                }
                comm.Parameters.Add("splogin", MySqlDbType.VarChar).Value = "1";
                comm.Parameters.Add("spsenha", MySqlDbType.VarChar).Value = "1";
                comm.Parameters.Add("spativo", MySqlDbType.Bit).Value = ativo;
                comm.Parameters.Add("spcod_nivel", MySqlDbType.Int32).Value = cod_nivel;
                comm.Parameters.Add("spinstituicao", MySqlDbType.Int32).Value = cod_instituicao;

                if (rgV &&
                     nomeV &&
                     telefoneV &&
                     cpfV)
                {
                    MessageBox.Show("Concluído!!");
                }
                id = Convert.ToInt32(comm.ExecuteScalar());
                comm.Connection.Close();
            }
            catch
            {
                if (rgV == false && nomeV == false && telefoneV == false && cpfV == false)
                    MessageBox.Show("Campos Inválidos");
                else if (rgV == false)
                    MessageBox.Show("Rg inválido");
                else if (nomeV == false)
                    MessageBox.Show("Nome inválido");
                else if (telefoneV == false)
                    MessageBox.Show("Telefone inválido");
                else if (cpfV == false)
                    MessageBox.Show("Cpf inválido");
            }
        }                 
                              
        /// <summary>
        /// metodo para alterar informações do responsavel
        /// </summary>
        public void Alterar()
        {

            bool rgV = false;
            bool nomeV = false;
            bool telefoneV = false;
            bool cpfV = false;
            var comm = Banco.Abrir();            

            try
            {
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "sp_responsavel_update";
                comm.Parameters.Add("spid", MySqlDbType.Int32).Value = Id;
                if (rg != string.Empty && rg.Length <= 12)
                {
                    if (valida.ValidarRG(rg))
                    {
                        rg = valida.RgFormat(rg);
                        comm.Parameters.Add("sprg", MySqlDbType.VarChar).Value = Rg;
                        rgV = true;
                    }
                }
                if (nome != string.Empty)
                {
                    if (valida.ValidarNome(nome))
                    {
                        comm.Parameters.Add("spnome", MySqlDbType.VarChar).Value = Nome;
                        nomeV = true;
                    }
                }
                if (telefone != string.Empty)
                {
                    if (valida.ValidarTelefone(telefone))
                    {
                        telefone = valida.TelefoneFormat(telefone);
                        comm.Parameters.Add("sptelefone", MySqlDbType.VarChar).Value = Telefone;
                        telefoneV = true;

                    }
                }


                if (cpf != string.Empty)
                {
                    if (valida.ValidarCpf(cpf))
                    {
                        cpf = valida.CpfFormat(cpf);
                        comm.Parameters.Add("spcpf", MySqlDbType.VarChar).Value = Cpf;
                        cpfV = true;
                    }
                }
                comm.Parameters.Add("splogin", MySqlDbType.VarChar).Value = "1";
                comm.Parameters.Add("spsenha", MySqlDbType.VarChar).Value = "1";
                comm.Parameters.Add("spativo", MySqlDbType.Bit).Value = Ativo;
                comm.Parameters.Add("spcod_nivel", MySqlDbType.Int32).Value = Cod_nivel;
                comm.Parameters.Add("spcod_instituicao", MySqlDbType.Int32).Value = Cod_instituicao;

                if (rgV &&
                     nomeV &&
                     telefoneV &&
                     cpfV)
                {
                    comm.ExecuteNonQuery();
                    MessageBox.Show("Concluído!!");

                }
            }
            catch
            {
                if (rgV == false)
                    MessageBox.Show("Rg inválido");
                else if (nomeV == false)
                    MessageBox.Show("Nome inválido");
                else if (telefoneV == false)
                    MessageBox.Show("Telefone inválido");
                else if (cpfV == false)
                    MessageBox.Show("Cpf inválido");
            }
            comm.Connection.Close();
        }
        /// <summary>
        /// metodo para consultar um responsavel pelo id
        /// </summary>
        /// <param name="_id"></param>

        /// <summary>
        /// metodo auxiliar ao botão inserir com ele responsaveis ja cadastrados porém desativados voltam a ser responsaveis ativos
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns></returns>
        public bool Conferir(string cpf)
        {
            if (valida.ValidarCpf(cpf))
            {
                cpf = valida.CpfFormat(cpf);
                if (CpfExistente(cpf) == 1)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// metodo que confere se o cpf ja existe
        /// </summary>
        /// <param name="_cpf"></param>
        /// <returns></returns>
        private int CpfExistente(string _cpf)
        {
            var comando = Banco.Abrir();
            comando.CommandText = "select count(cpf) from responsavel where cpf = '" + _cpf + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont2 = dr.GetInt32(0);
            }
            return cont2;
        }

        /// <summary>
        /// metodo para ver se o telefone n existe
        /// </summary>
        /// <param name="_telefone"></param>
        /// <returns></returns>
        private int TelefoneExiste(string _telefone)
        {
            var comando = Banco.Abrir();
            comando.CommandText = "select count(telefone) from responsavel where telefone = '" + _telefone + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont3 = dr.GetInt32(0);
            }
            return cont3;
        }

        /// <summary>
        /// metodo que identifica se o rg ja existe no banco
        /// </summary>
        /// <param name="_rg"></param>
        /// <returns></returns>
        public int RgExistente(string _rg)
        {

            var comando = Banco.Abrir();
            comando.CommandText = "select count(rg) from responsavel where rg = '" + _rg + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont = dr.GetInt32(0);
            }
            return cont;
        }

        public void ConsultarPorId(string _id) // declarar método
        { //implementar método
            Convert.ToInt32(_id);
            var comm = Banco.Abrir();
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "sp_responsavel_select_id";
            comm.Parameters.Add("spid", MySqlDbType.Int32).Value = _id;
            var dr = comm.ExecuteReader();
            while (dr.Read())
            {
                this.Rg = dr.GetString(0);
                this.Nome = dr.GetString(1);
                this.Telefone = dr.GetString(2);
                this.Cpf = dr.GetString(3);
                this.Cod_instituicao = dr.GetInt32(4);
            }
            comm.Connection.Close();
        }
        /// <summary>
        /// metodo para consultar todos responsaveis
        /// </summary>
        /// <returns></returns>
        public MySqlDataReader ConsultarTodos()
        {
            var comm = Banco.Abrir();
            comm.CommandText = "select * from responsavel";
            var dr = comm.ExecuteReader();
            return dr;
        }
        /// <summary>
        /// metodo para listar tudo no datagridview
        /// </summary>
        /// <returns></returns>
        public DataTable ListarGrid()
        {
            var comm = Banco.Abrir();
            string strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where ativo = true;";
            string strCon = @"server=127.0.0.1;user id=root;database=escola";
            MySqlConnection cn = new MySqlConnection(strCon);
            MySqlCommand objCommand = new MySqlCommand(strComm, cn);
            MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
            DataTable dt = new DataTable();
            objAdapter.Fill(dt);
            return dt;
        }
        /// <summary>
        /// metodo que utiliza o cpf para identificar o id do responsavel
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns></returns>
        public int ConsultarId(string cpf) // declarar método
        { //implementar método
            var comm = Banco.Abrir();
            comm.CommandText = "select id from responsavel where cpf = '" + cpf + "'";
            var dr = comm.ExecuteReader();
            while (dr.Read())
            {
                this.id = dr.GetInt32(0);
            }
            return id;
        }
        string strComm = "";
        /// <summary>
        /// metodo para pesquisar responsaveis pelo telefone, nome, instituição e cpf
        /// </summary>
        /// <param name="quaisquer"></param>
        /// <returns></returns>
        public DataTable Pesquisa(string quaisquer)
        {            
            var comm = Banco.Abrir();
            try
            {
                if (valida.ValidarNome(quaisquer) && valida.ValidarInstituicao(quaisquer) == 0)
                    strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where r.nome = '" + quaisquer + "' and r.ativo = true;";
                else if (valida.ValidarCpf(quaisquer))
                    strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where r.cpf = '" + quaisquer + "' and r.ativo = true;";
                else if (valida.ValidarTelefone(quaisquer))
                    strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where r.telefone = '" + quaisquer + "' and r.ativo = true;";
                else if (valida.ValidarInstituicao(quaisquer) != 0)
                    strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where inst.nome = '" + quaisquer + "' and r.ativo = true;";
                string strCon = @"server=127.0.0.1;user id=root;database=escola";
                MySqlConnection cn = new MySqlConnection(strCon);
                MySqlCommand objCommand = new MySqlCommand(strComm, cn);
                MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
                DataTable dt = new DataTable();
                objAdapter.Fill(dt);
                return dt;
            }
            catch
            {
                strComm = "select r.id as ID, r.nome as Nome,r.telefone as Telefone,r.cpf as CPF, inst.nome as Instituicao from Responsavel as r inner join instituicao as inst on r.cod_instituicao = inst.id where r.nome = 'limpo';";
                string strCon = @"server=127.0.0.1;user id=root;database=escola";
                MySqlConnection cn = new MySqlConnection(strCon);
                MySqlCommand objCommand = new MySqlCommand(strComm, cn);
                MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
                DataTable dt = new DataTable();
                objAdapter.Fill(dt);
                return dt;
            };
        }
       
    }
 }

