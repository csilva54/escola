﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Escola2
{
    public class Instituicao
    {
        private int id;
        private string nome;
        
        public int Id { get => id; set => id = value; }
        public string Nome { get => nome; set => nome = value; }
        

        /// <summary>
        /// Método Construtor Vazio da classe.
        /// </summary>
        public Instituicao()
        {

        }
        /// <summary>
        /// Método Construtor com todos os parâmetros da classe.
        /// </summary>
        public Instituicao(int _id, string _nome)
        {
            this.Id = _id;
            this.Nome = _nome;
        }
        /// <summary>
        /// Método Construtor da classe com apenas o parâmetro da descrição.
        /// </summary>
        public Instituicao(string _nome)
        {
            this.Nome = _nome;
        }
        /// <summary>
        /// Este método serve para Inserir uma Instituição.Retorna uma váriavel booleana determinando se a operação foi executada ou não.
        /// </summary>
        /// <returns></returns>
        public bool Inserir()
        {
            bool validacao = false;
            try
            {
                var comando = Banco.Abrir();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "sp_instituicao_inserir";                
                comando.Parameters.Add("spnome", MySqlDbType.Text).Value = Nome;                
                this.id = Convert.ToInt32(comando.ExecuteScalar());
                MessageBox.Show("Concluído!!");
                validacao = true;

            }
            catch (Exception)
            {
                MessageBox.Show("Erro para Inserir Instituição!!");
                validacao = false;
            }

            return validacao;
        }

        /// <summary>
        ///  Este método altera a Instituição especificada pelo Id.Retorna uma váriavel booleana determinando se a operação foi executada ou não.
        /// </summary>
        /// <returns></returns>
        public bool Alterar()
        {
            bool validacao = false; //variavel de validação da execução da alteração do Evento
            if (id > 0)
            {
                try //usando o try para a tentativa de execução para caso dê erro possamos tratar
                {
                    var comando = Banco.Abrir();
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandText = "sp_instituicao_update";
                    comando.Parameters.Add("spid", MySqlDbType.Text).Value = Id;
                    comando.Parameters.Add("spnome", MySqlDbType.Text).Value = Nome;
                    comando.ExecuteNonQuery();
                    validacao = true; //retornando verdadeiro mostrando que a alteração foi feita corretamente
                }
                catch (Exception)
                {
                    validacao = false; //retornando falso mostrando que deu erro na alteração (possivelmente algum campo preenchido errado)
                }

            }

            return validacao;
        }

        public bool Deletar()
        {

            bool validacao = false;
            try //usando o try para a tentativa de execução para caso dê erro possamos tratar
            {
                var comando = Banco.Abrir();
                comando.CommandText = "delete from instituicao where id = " + Id + "";
                var dr = comando.ExecuteReader();
                Banco.Fechar(comando);
                validacao = true;
            }
            catch (Exception)
            {
                validacao = false; //retornando falso mostrando que deu erro na alteração (possivelmente algum campo preenchido errado)
            }

            return validacao;
        }

        /// <summary>
        /// Este método consulta todos os Eventos presentes no Banco.
        /// </summary>
        public MySqlDataReader ConsultarTodos()
        {
            var comm = Banco.Abrir();
            comm.CommandText = "select * from instituicao;";
            var dr = comm.ExecuteReader();
            return dr;
        }
        /// <summary>
        /// Este método necessita apenas do parâmetros Id para fazer uma consulta pelo Id do Evento.Retorna uma váriavel booleana determinando se a operação foi executada ou não.
        /// </summary>
        public bool ConsultarPorId(int _id)
        {
            bool validacao = false; //variavel de validação usada para determinar se a operação de consulta foi executada.
            try
            {
                var comando = Banco.Abrir();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "sp_instituicao_select_id";
                comando.Parameters.Add("spid", MySqlDbType.Int32).Value = _id;
                var dr = comando.ExecuteReader();
                while (dr.Read())
                {
                    this.Id = dr.GetInt32(0);
                    this.nome = dr.GetString(1);
                }
                validacao = true;
            }
            catch (Exception)
            {
                validacao = false;
            }
            return validacao;
        }

        public int ConsultarPorNome(string nome)
        {
            var comando = Banco.Abrir();
            comando.CommandText = "select * from instituicao where nome = '" + nome + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                this.Id = dr.GetInt32(0);
            }
            return Id;

        }

        /// <summary>
        /// Este método consulta todos as Instituições presentes no Banco.
        /// </summary>
        //public MySqlDataReader ConsultaNome(string _nome)
        //{
        //    var comando = Banco.Abrir();
        //    this.nome = _nome;
        //    comando.CommandText = "select nome from instituicao";
        //    var dr = comando.ExecuteReader();

        //    _nome = nome;
        //    return dr;

        //}
        //public DataTable PreencherComboBox(){
        //    Instituicao instituicao = new Instituicao();
        //    var comm = Banco.Abrir();
        //    DataTable dt = new DataTable();
        //    comm.CommandText = "select nome from instituicao;";
        //    string strConn = @"server=127.0.0.1;user id=root;database=escola";
        //    MySqlDataAdapter adapter = new MySqlDataAdapter("select nome from instituicao;", strConn);
        //    adapter.Fill(dt);
        //    return dt;
        //}

    }
}

