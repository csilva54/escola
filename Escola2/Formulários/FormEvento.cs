﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Escola2.Formulários
{
    public partial class FormEvento : Form
    {
        public FormEvento()
        {
            InitializeComponent();            
        }

        private void cbInstituicao_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void FormPublicar_Load(object sender, EventArgs e)
        {

            Instituicao i = new Instituicao();
            DataTable dtpi = new DataTable();
            dtpi.Load(i.ConsultarTodos());
            cbInstituicao.DataSource = dtpi;
            cbInstituicao.DisplayMember = "nome";
            cbInstituicao.ValueMember = "id";
            
            if (txtDataCadastro.Text == string.Empty)
            {
                txtDescricao.Enabled = false;
                txtnome.Enabled = false;
                txtDataEvento.Enabled = false;
                txtCodigo.Enabled = false;
                cbInstituicao.Enabled = false;
                cbAtivo.Enabled = false;
                dtEvento.Enabled = false;
                dtEventoh.Enabled = false;
                btnPublicar.Enabled = false;
                btnSalvar.Enabled = false;
                btnExcluir.Enabled = false;
                btnEditar.Enabled = false;
                btnCancelar.Enabled = false;
                btnCadastrar.Enabled = true;
                dtgEvento.Enabled = true;
                dtgEvento.AutoResizeRows();
                
                txtDescricao.Clear();
                txtnome.Clear();
                txtSituacao.Clear();
                txtDataCadastro.Clear();
                dtEventoh.ResetText();
                dtEvento.ResetText();
                cbInstituicao.ResetText();
            }
        }

        private void btnPublicar_Click(object sender, EventArgs e)
        {
            Evento evento = new Evento();
            Instituicao inst = new Instituicao();

            var hora = Convert.ToString(dtEventoh.Value.TimeOfDay);
            var data = Convert.ToString(dtEvento.Value.Date).Substring(0, 10);

            evento.Nome = txtnome.Text;
            evento.Descricao = txtDescricao.Text;
            evento.DataEvento = Convert.ToDateTime(data + " " + hora);
            evento.Ativo = cbAtivo.Checked;
            evento.DataCadastro = DateTime.Now;
            evento.CodigoInstituicao = inst.ConsultarPorNome(cbInstituicao.Text);
            var Inserir = evento.Inserir();
            if (Inserir)
            {
                MessageBox.Show("Evento Cadastrado com sucesso!");

                btnlimpa_Click(sender, e);
                FormPublicar_Load(sender, e);
                button1_Click(sender, e);
            }
            else
            {
                MessageBox.Show("Falha ao inserir evento!");
            }

            FormPublicar_Load(sender, e);
        }
        private void btnEditar_Click(object sender, EventArgs e)
        {
            txtDescricao.Enabled = true;
            txtnome.Enabled = true;            
            cbInstituicao.Enabled = true;
            cbAtivo.Enabled = true;
            dtEvento.Enabled = true;
            dtEventoh.Enabled = true;
            btnSalvar.Enabled = true;
            btnPublicar.Enabled = false;
            btnExcluir.Enabled = true;
            btnCadastrar.Enabled = false;
            btnCancelar.Enabled = true;            
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

            if (txtCodigo.Text != string.Empty)
            {
                Evento evento = new Evento();
                Instituicao inst = new Instituicao();

                var hora = Convert.ToString(dtEventoh.Value.TimeOfDay);
                var data = Convert.ToString(dtEvento.Value.Date).Substring(0, 10);

                evento.Id = int.Parse(txtCodigo.Text);
                evento.Nome = txtnome.Text;
                evento.Descricao = txtDescricao.Text;
                evento.DataEvento = Convert.ToDateTime(data + " " + hora);
                evento.Ativo = cbAtivo.Checked;
                evento.DataCadastro = DateTime.Now;
                evento.CodigoInstituicao = inst.ConsultarPorNome(cbInstituicao.Text);
                var Alterar = evento.Alterar();
                if (Alterar)
                {
                    MessageBox.Show("Alteracao realizada com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);                   

                    btnlimpa_Click(sender, e);
                    FormPublicar_Load(sender, e);
                    button1_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Falha na alteracao.");
                }
            }
         
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Convert.ToInt32(dtgEvento.Rows[e.RowIndex].Cells[e.ColumnIndex].ColumnIndex) <= 0)
            {

                btnEditar.Enabled = true;
                btnlimpa_Click(sender, e);

                Evento evento = new Evento();
                Instituicao instituicao = new Instituicao();

                btnEditar.Enabled = true;
                int id = Convert.ToInt32(dtgEvento.Rows[e.RowIndex].Cells[1].Value);
                evento.ConsultarPorId(id);
                txtCodigo.Text = evento.Id.ToString();
                txtnome.Text = evento.Nome;
                txtDescricao.Text = evento.Descricao;
                txtDataCadastro.Text = evento.DataCadastro.ToString();
                txtDataEvento.Text = evento.DataEvento.ToString();

                if (evento.Ativo)
                {
                    txtSituacao.Text = "Ativo";
                    cbAtivo.Checked = true;
                }
                else
                {
                    txtSituacao.Text = "Inativo";
                    cbAtivo.Checked = false;
                }

                instituicao.ConsultarPorId(evento.CodigoInstituicao);
                var inst = instituicao.Nome;
                cbInstituicao.SelectedIndex = cbInstituicao.FindStringExact(inst);

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

            Evento evento = new Evento();

            dtgEvento.DataSource = evento.ListarGrid();
            txtpesquisarEve.Text = string.Empty;
            dtgEvento.AutoResizeRows();
          
        }

        private void btnlimpa_Click(object sender, EventArgs e)
        {
            txtDescricao.Clear();
            txtnome.Clear();
            txtSituacao.Clear();
            txtDataCadastro.Clear();
            txtDataEvento.Clear();
            txtCodigo.Clear();
            dtEventoh.ResetText();
            dtEvento.ResetText();
            cbInstituicao.ResetText();
            cbAtivo.Checked = false;
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            btnlimpa_Click(sender, e);

            btnCadastrar.Enabled = false;
            btnEditar.Enabled = false;
            btnExcluir.Enabled = false;
            btnSalvar.Enabled = false;            
            txtDescricao.Enabled = true;
            txtnome.Enabled = true;
            cbInstituicao.Enabled = true;
            cbAtivo.Enabled = true;
            dtEvento.Enabled = true;
            dtEventoh.Enabled = true;
            btnPublicar.Enabled = true;
            btnCancelar.Enabled = true;
            dtgEvento.Enabled = false;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            btnlimpa_Click(sender, e);
            FormPublicar_Load(sender, e);
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Tem certeza que deseja excluir o evento?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {

                Evento evento = new Evento();

                evento.Id = int.Parse(txtCodigo.Text);
                var Deletado = evento.Deletar();

                if (Deletado)
                {
                    //Confirmando exclusão para o usuário
                    MessageBox.Show("Evento apagado com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    btnlimpa_Click(sender, e);
                    FormPublicar_Load(sender, e);
                    button1_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Falha ao excluir evento.");
                }               

            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            Evento evento = new Evento();
            dtgEvento.DataSource = evento.Pesquisa(txtpesquisarEve.Text);
        }

        private void txtpesquisarEve_TextChanged(object sender, EventArgs e)
        {
            if (txtpesquisarEve.Text != string.Empty)
            {
                Evento evento = new Evento();
                dtgEvento.DataSource = evento.Pesquisa(txtpesquisarEve.Text);
            }
            else
            {
                button1_Click(sender, e);
                dtgEvento.Refresh();   
            }
        }
    }
}
