﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Escola2.Formulários
{
    public partial class FormResponsavel : Form
    {
        public FormResponsavel()
        {
            InitializeComponent();
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Click do datagridview, quando o usuário clicar em um cpf as informações do mesmo aparecerão nos texts boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DtgResponsavel_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Responsave r = new Responsave();
            string cpf = Convert.ToString(dtgResponsavel.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
            txtID.Text = r.ConsultarId(cpf).ToString();
            cpf = string.Empty;
            r.ConsultarPorId(txtID.Text);
            txtCpf.Text = r.Cpf;
            txtNome.Text = r.Nome;
            txtRg.Text = r.Rg;
            txtTelefone.Text = r.Telefone;
            if (r.Cod_instituicao == 1)
                cmbInstituicao.Text = "Penha";
            else if (r.Cod_instituicao == 2)
                cmbInstituicao.Text = "Lider";
        }
        /// <summary>
        /// Click do botao inserir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnInserir_Click(object sender, EventArgs e)
        {
            if (txtID.Text == string.Empty)
            {
                int num = 0;
                Responsave responsavel = new Responsave();
                Valida valida = new Valida();
                responsavel.Rg = txtRg.Text;
                responsavel.Nome = txtNome.Text;
                responsavel.Telefone = txtTelefone.Text;
                responsavel.Cpf = txtCpf.Text;
                responsavel.Ativo = Convert.ToBoolean(1);
                responsavel.Cod_nivel = Convert.ToInt32(1);
                responsavel.Cod_instituicao = Convert.ToInt32(cmbInstituicao.SelectedValue);
                /* aqui é onde o sistema verifica se o usuario já é existente, caso seja existente o sistema irá alterar o "ativo"
                 ao inves de inserir um novo usuario (essa verificação é feita através do cpf)
                */
                if (responsavel.Conferir(txtCpf.Text))
                {
                    responsavel.Inserir();
                    BtnClear_Click(sender, e);
                }
                else
                {
                    responsavel.Id = responsavel.ConsultarId(txtCpf.Text);
                    responsavel.Alterar();
                    BtnClear_Click(sender, e);
                }
            }
            if (txtID.Text != string.Empty)
            {
                txtID.Text = string.Empty; 
            }
        }
        /// <summary>
        /// Irá garantir que o programa abra com as informações de instituição no comboBox
        ///(Será alterado em breve, pois o mesmo não está trabalhando com o banco)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormResponsavel_Load(object sender, EventArgs e)
        {
            Instituicao i = new Instituicao();
            DataTable dtp = new DataTable();
            dtp.Load(i.ConsultarTodos());
            cmbInstituicao.DataSource = dtp;
            cmbInstituicao.DisplayMember = "nome";
            cmbInstituicao.ValueMember = "id";

        }

        private void CmbInstituicao_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Click do botão "listar", o que irá listar todos os responsáveis no datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, EventArgs e)
        {
            Responsave r = new Responsave();
            
            dtgResponsavel.DataSource = r.ListarGrid();
            txtPesq.Text = string.Empty;
        }
        /// <summary>
        /// Click do botão que irá alterar um usuário ja existente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAlterar_Click(object sender, EventArgs e)
        {
            int num = 0;
            Responsave responsavel = new Responsave();
            if (txtID.Text != string.Empty)
            {
                responsavel.Id = int.Parse(txtID.Text);
                responsavel.Rg = txtRg.Text;
                responsavel.Nome = txtNome.Text;
                responsavel.Telefone = txtTelefone.Text;
                responsavel.Cpf = txtCpf.Text;
                responsavel.Ativo = Convert.ToBoolean(1);
                responsavel.Cod_nivel = Convert.ToInt32(1);            
                responsavel.Cod_instituicao = Convert.ToInt32(cmbInstituicao.SelectedValue);
                responsavel.Alterar();
                BtnClear_Click(sender, e);
            }
        }
        /// <summary>
        /// Ao adquirir o id do responsável utilizando o cpf no datagridview, uma consulta irá ocorrer dos seus dados através do id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            if (txtID.Text != string.Empty)
            {
                Responsave r = new Responsave();
                r.ConsultarId(txtID.Text);
                txtCpf.Text = r.Cpf;
                txtNome.Text = r.Nome;
                txtRg.Text = r.Rg;
                txtTelefone.Text = r.Telefone;
            }
        }
        /// <summary>
        /// ira modificar o campo "ativo" do responsavel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnExcluir_Click(object sender, EventArgs e)
        {
            if(txtID.Text != string.Empty)
            {
                Responsave responsavel = new Responsave();
                responsavel.Id = int.Parse(txtID.Text);
                responsavel.Rg = txtRg.Text;
                responsavel.Nome = txtNome.Text;
                responsavel.Telefone = txtTelefone.Text;
                responsavel.Cpf = txtCpf.Text;
                responsavel.Ativo = Convert.ToBoolean(0);
                responsavel.Cod_nivel = Convert.ToInt32(1);
                responsavel.Cod_instituicao = Convert.ToInt32(cmbInstituicao.SelectedValue);
                responsavel.Alterar();
                BtnClear_Click(sender, e);
            }
        }
        /// <summary>
        /// fecha a aplicação
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// efetua a pesquida do usuário
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPesq_Click(object sender, EventArgs e)
        {
            Responsave r = new Responsave();

            dtgResponsavel.DataSource = r.Pesquisa(txtPesq.Text);
            
        }
        /// <summary>
        /// Limpa todos os campos digitados
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClear_Click(object sender, EventArgs e)
        {
            txtID.Text = string.Empty;
            txtTelefone.Text = string.Empty;
            txtRg.Text = string.Empty;
            txtCpf.Text = string.Empty;
            txtNome.Text = string.Empty;
            cmbInstituicao.Text = "Penha";
        }

        private void Label7_Click(object sender, EventArgs e)
        {

        }

        private void TxtPesq_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtTelefone_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtRg_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtCpf_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
