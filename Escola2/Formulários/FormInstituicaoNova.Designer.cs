﻿namespace Escola2.Formulários
{
    partial class FormInstituicaoNova
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbInstituicao = new System.Windows.Forms.GroupBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnListar = new System.Windows.Forms.Button();
            this.btnInserir = new System.Windows.Forms.Button();
            this.picbLogo = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInstituicao = new System.Windows.Forms.TextBox();
            this.ID = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.grpListaInstituicao = new System.Windows.Forms.GroupBox();
            this.dtgvInstituicao = new System.Windows.Forms.DataGridView();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grbInstituicao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbLogo)).BeginInit();
            this.grpListaInstituicao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvInstituicao)).BeginInit();
            this.SuspendLayout();
            // 
            // grbInstituicao
            // 
            this.grbInstituicao.Controls.Add(this.btnCancelar);
            this.grbInstituicao.Controls.Add(this.btnExcluir);
            this.grbInstituicao.Controls.Add(this.btnAlterar);
            this.grbInstituicao.Controls.Add(this.btnListar);
            this.grbInstituicao.Controls.Add(this.btnInserir);
            this.grbInstituicao.Controls.Add(this.picbLogo);
            this.grbInstituicao.Controls.Add(this.label1);
            this.grbInstituicao.Controls.Add(this.txtInstituicao);
            this.grbInstituicao.Controls.Add(this.ID);
            this.grbInstituicao.Controls.Add(this.txtID);
            this.grbInstituicao.Location = new System.Drawing.Point(53, 12);
            this.grbInstituicao.Name = "grbInstituicao";
            this.grbInstituicao.Size = new System.Drawing.Size(582, 167);
            this.grbInstituicao.TabIndex = 0;
            this.grbInstituicao.TabStop = false;
            this.grbInstituicao.Text = "Instituição";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(490, 112);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(80, 40);
            this.btnCancelar.TabIndex = 6;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(240, 112);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(80, 40);
            this.btnExcluir.TabIndex = 7;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.BtnExcluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Location = new System.Drawing.Point(131, 112);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(80, 40);
            this.btnAlterar.TabIndex = 8;
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.BtnAlterar_Click);
            // 
            // btnListar
            // 
            this.btnListar.Location = new System.Drawing.Point(347, 112);
            this.btnListar.Name = "btnListar";
            this.btnListar.Size = new System.Drawing.Size(80, 40);
            this.btnListar.TabIndex = 9;
            this.btnListar.Text = "&Listar";
            this.btnListar.UseVisualStyleBackColor = true;
            this.btnListar.Click += new System.EventHandler(this.btnListar_Click);
            // 
            // btnInserir
            // 
            this.btnInserir.Location = new System.Drawing.Point(29, 112);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(80, 40);
            this.btnInserir.TabIndex = 10;
            this.btnInserir.Text = "&Inserir";
            this.btnInserir.UseVisualStyleBackColor = true;
            this.btnInserir.Click += new System.EventHandler(this.BtnInserir_Click);
            // 
            // picbLogo
            // 
            this.picbLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picbLogo.Location = new System.Drawing.Point(482, 10);
            this.picbLogo.Name = "picbLogo";
            this.picbLogo.Size = new System.Drawing.Size(88, 87);
            this.picbLogo.TabIndex = 5;
            this.picbLogo.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nome: ";
            // 
            // txtInstituicao
            // 
            this.txtInstituicao.Location = new System.Drawing.Point(51, 48);
            this.txtInstituicao.Name = "txtInstituicao";
            this.txtInstituicao.Size = new System.Drawing.Size(269, 20);
            this.txtInstituicao.TabIndex = 2;
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Location = new System.Drawing.Point(5, 25);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(18, 13);
            this.ID.TabIndex = 1;
            this.ID.Text = "ID";
            // 
            // txtID
            // 
            this.txtID.Enabled = false;
            this.txtID.Location = new System.Drawing.Point(29, 22);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(40, 20);
            this.txtID.TabIndex = 0;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            // 
            // grpListaInstituicao
            // 
            this.grpListaInstituicao.Controls.Add(this.dtgvInstituicao);
            this.grpListaInstituicao.Location = new System.Drawing.Point(53, 185);
            this.grpListaInstituicao.Name = "grpListaInstituicao";
            this.grpListaInstituicao.Size = new System.Drawing.Size(582, 184);
            this.grpListaInstituicao.TabIndex = 1;
            this.grpListaInstituicao.TabStop = false;
            this.grpListaInstituicao.Text = "Lista de Instituições";
            // 
            // dtgvInstituicao
            // 
            this.dtgvInstituicao.AllowUserToAddRows = false;
            this.dtgvInstituicao.AllowUserToDeleteRows = false;
            this.dtgvInstituicao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvInstituicao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome});
            this.dtgvInstituicao.Location = new System.Drawing.Point(6, 28);
            this.dtgvInstituicao.Name = "dtgvInstituicao";
            this.dtgvInstituicao.ReadOnly = true;
            this.dtgvInstituicao.Size = new System.Drawing.Size(570, 150);
            this.dtgvInstituicao.TabIndex = 0;
            // 
            // Nome
            // 
            this.Nome.Frozen = true;
            this.Nome.HeaderText = "Instituição";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            this.Nome.Width = 525;
            // 
            // FormInstituicaoNova
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 375);
            this.Controls.Add(this.grpListaInstituicao);
            this.Controls.Add(this.grbInstituicao);
            this.Name = "FormInstituicaoNova";
            this.Text = "FormInstituicaoNova";
            this.Load += new System.EventHandler(this.FormInstituicaoNova_Load);
            this.grbInstituicao.ResumeLayout(false);
            this.grbInstituicao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbLogo)).EndInit();
            this.grpListaInstituicao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvInstituicao)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbInstituicao;
        private System.Windows.Forms.GroupBox grpListaInstituicao;
        private System.Windows.Forms.DataGridView dtgvInstituicao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInstituicao;
        private System.Windows.Forms.Label ID;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.PictureBox picbLogo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnListar;
        private System.Windows.Forms.Button btnInserir;
    }
}