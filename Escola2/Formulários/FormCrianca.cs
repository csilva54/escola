﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Escola2.Formulários
{
    public partial class FormCrianca : Form
    {
        public FormCrianca()
        {
            InitializeComponent();
        }

        public void BtnInserir_Click(object sender, EventArgs e)
        {
            if (txtID.Text == string.Empty)
            {
                Crianca crianca = new Crianca();
                crianca.Nome = txtNome.Text;
                crianca.Data_nascimento = txtDataNacimento.Text;
                crianca.Ativo = Convert.ToBoolean(1);
                Responsave responsave = new Responsave();
                crianca.Id_responsavel = responsave.ConsultarId(txtCpf.Text);

            }
        }

        public void BtnAlterar_Click(object sender, EventArgs e)
        {
            Crianca crianca = new Crianca();
            Responsave responsave = new Responsave();
            if (txtID.Text!= string.Empty)
            {
                crianca.Id = int.Parse(txtID.Text);
                crianca.Nome = txtNome.Text;
                responsave.Cpf = txtCpf.Text;
            }
        }

        private void BtnExcluir_Click(object sender, EventArgs e)
        {
            if (txtID.Text != string.Empty)
            {
                Crianca crianca = new Crianca();
                crianca.Nome = txtNome.Text;
                crianca.Data_nascimento = txtDataNacimento.Text;
                crianca.Ativo = Convert.ToBoolean(1);
                Responsave responsave = new Responsave();
                crianca.Id_responsavel = responsave.ConsultarId(txtCpf.Text);
            }
        }

        private void BtnListar_Click(object sender, EventArgs e)
        {
            Crianca cri = new Crianca();
            dataGridView1.DataSource = cri.ListarGrid();
            txtPesq.Text = string.Empty;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            txtID.Text = string.Empty;
            txtCpf.Text = string.Empty;
            txtNome.Text = string.Empty;

        }

        private void BtnPesq_Click(object sender, EventArgs e)
        {
            Crianca cri = new Crianca();
            dataGridView1.DataSource = cri.Pesquisa(txtPesq.Text);
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
