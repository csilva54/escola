﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Escola2.Formulários
{
    public partial class FormInstituicaoNova : Form
    {
        public FormInstituicaoNova()
        {
            InitializeComponent();
        }

        private void FormInstituicaoNova_Load(object sender, EventArgs e)
        {
            
        }

        private void BtnInserir_Click(object sender, EventArgs e)
        {
            if (txtID.Text == string.Empty)
            {
                Instituicao instituicao = new Instituicao();                                
                instituicao.Nome = txtInstituicao.Text;                
                instituicao.Inserir();
            }
           
        }

        private void BtnAlterar_Click(object sender, EventArgs e)
        {
            Instituicao instituicao = new Instituicao();
            if (txtID.Text != string.Empty)
            {
                instituicao.Id = int.Parse(txtID.Text);
                instituicao.Nome = txtInstituicao.Text;                
                instituicao.Alterar();
            }
        }

        private void BtnExcluir_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Tem certeza que deseja excluir essa Instituicao?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                if (txtID.Text != string.Empty)
                {
                    Instituicao instituicao = new Instituicao();
                    instituicao.Id = int.Parse(txtID.Text);
                    var Deletado = instituicao.Deletar();

                    if (Deletado)
                    {
                        //Confirmando exclusão para o usuário
                        MessageBox.Show("Instituicao apagada com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);                        
                    }
                    else
                    {
                        MessageBox.Show("Falha ao excluir Instituicao.");
                    }
                }
            }
        }

        private void btnListar_Click(object sender, EventArgs e)
        {

        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
