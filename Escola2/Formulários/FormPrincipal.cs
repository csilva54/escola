﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Escola2.Formulários
{
    public partial class FormPrincipal : Form
    {
        public FormPrincipal()
        {
            InitializeComponent();
            AbrirFormNoPanel<FormHome>();
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            
        }

        private void btnEvento_Click(object sender, EventArgs e)
        {
            AbrirFormNoPanel<FormEvento>();
        }

        private void btnCrianca_Click(object sender, EventArgs e)
        {
            AbrirFormNoPanel<FormCrianca>();
        }

        private void AbrirFormNoPanel<Forms>() where Forms : Form, new()
        {
            Form formulario;
            formulario = panelConteudo.Controls.OfType<Forms>().FirstOrDefault();

            if (formulario == null)
            {
                formulario = new Forms();
                formulario.TopLevel = false;
                formulario.FormBorderStyle = FormBorderStyle.None;
                formulario.Dock = DockStyle.Fill;
                panelConteudo.Controls.Add(formulario);
                panelConteudo.Tag = formulario;
                formulario.Show();
                formulario.BringToFront();
            }
            else if (formulario != null) {

                formulario.Close();
                formulario = new Forms();
                formulario.TopLevel = false;
                formulario.FormBorderStyle = FormBorderStyle.None;
                formulario.Dock = DockStyle.Fill;
                panelConteudo.Controls.Add(formulario);
                panelConteudo.Tag = formulario;
                formulario.Show();
                formulario.BringToFront();
            }
            else
            {
                if (formulario.WindowState == FormWindowState.Minimized)
                {
                    formulario.WindowState = FormWindowState.Normal;
                    formulario.BringToFront();
                }
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnInstituicao_Click(object sender, EventArgs e)
        {
            AbrirFormNoPanel<FormInstituicaoNova>();
        }

        private void btnFuncionario_Click(object sender, EventArgs e)
        {
            AbrirFormNoPanel<FormFuncionarioNovo>();
        }

        private void btnResponsavel_Click(object sender, EventArgs e)
        {
            AbrirFormNoPanel<FormResponsavel>();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            AbrirFormNoPanel<FormHome>();
        }

        private void panelConteudo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelTopo_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
