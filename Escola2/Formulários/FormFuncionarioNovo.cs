﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Escola2.Formulários
{
    public partial class FormFuncionarioNovo : Form
    {
        public FormFuncionarioNovo()
        {
            InitializeComponent();
        }

        private void FormFuncionarioNovo_Load(object sender, EventArgs e)
        {
            Instituicao i = new Instituicao();
            DataTable dtpi = new DataTable();
            dtpi.Load(i.ConsultarTodos());
            cmbInstituicao.DataSource = dtpi;
            cmbInstituicao.DisplayMember = "nome";
            cmbInstituicao.ValueMember = "id";

            // AJUSTAR ESTAS REFERENCIAS 
            //Cargo c = new Cargo();
            //DataTable dtpc = new DataTable();
            //dtpc.Load(c.ConsultarTodos());
            //cmbInstituicao.DataSource = dtpc;
            //cmbInstituicao.DisplayMember = "nome";
            //cmbInstituicao.ValueMember = "id";
            //Nivel n = new Nivel();
            //DataTable dtpn = new DataTable();
            //dtpn.Load(n.ConsultarTodos());
            //cmbInstituicao.DataSource = dtpn;
            //cmbInstituicao.DisplayMember = "nome";
            //cmbInstituicao.ValueMember = "id";
        }

        private void Label8_Click(object sender, EventArgs e)
        {

        }

        private void BtnListar_Click(object sender, EventArgs e)
        {

        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            txtID.Text = string.Empty;
            txtTelefone.Text = string.Empty;
            txtRg.Text = string.Empty;
            txtCpf.Text = string.Empty;
            txtNome.Text = string.Empty;
            txtNascimento.Text = string.Empty;
        }

        private void BtnInserir_Click(object sender, EventArgs e)
        {
            if (txtID.Text == string.Empty)
            {
                Funcionario funcionario = new Funcionario();
                funcionario.Rg = txtRg.Text;
                funcionario.Nome = txtNome.Text;
                funcionario.Telefone_funcionario = txtTelefone.Text;
                funcionario.Cpf = txtCpf.Text;
                funcionario.Ativo = Convert.ToBoolean(1);
                funcionario.Cod_nivel = Convert.ToInt32(cmbNivel.SelectedValue);
                funcionario.Cod_instituicao = Convert.ToInt32(cmbInstituicao.SelectedValue);
                funcionario.Cod_cargo = Convert.ToInt32(cmbCargo.SelectedValue);


                /* aqui é onde o sistema verifica se o usuario já é existente, caso seja existente o sistema irá alterar o "ativo"
                 ao inves de inserir um novo usuario (essa verificação é feita através do cpf)
                */
                if (funcionario.Conferir(txtCpf.Text))
                {
                    funcionario.Inserir();
                    BtnClear_Click(sender, e);
                }
                else
                {
                    funcionario.Id = funcionario.ConsultarId(txtCpf.Text);
                    funcionario.Alterar();
                    BtnClear_Click(sender, e);
                }
            }
            if (txtID.Text != string.Empty)
            {
                txtID.Text = string.Empty;
            }
        }

        private void DtgResponsavel_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Funcionario f = new Funcionario();
            string cpf = Convert.ToString(dtgResponsavel.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
            txtID.Text = f.ConsultarId(cpf).ToString();
            cpf = string.Empty;
            f.ConsultarPorId(int.Parse(txtID.Text));
            txtCpf.Text = f.Cpf;
            txtNome.Text = f.Nome;
            txtRg.Text = f.Rg;
            txtTelefone.Text = f.Telefone_funcionario;
            if (f.Cod_instituicao == 1)
                cmbInstituicao.Text = "Penha";
            else if (f.Cod_instituicao == 2)
                cmbInstituicao.Text = "Lider";
        }

        private void BtnAlterar_Click(object sender, EventArgs e)
        {
            Funcionario funcionario = new Funcionario();
            if (txtID.Text != string.Empty)
            {
                funcionario.Id = int.Parse(txtID.Text);
                funcionario.Rg = txtRg.Text;
                funcionario.Nome = txtNome.Text;
                funcionario.Telefone_funcionario = txtTelefone.Text;
                funcionario.Cpf = txtCpf.Text;
                funcionario.Ativo = Convert.ToBoolean(1);
                funcionario.Cod_nivel = Convert.ToInt32(1);
                funcionario.Cod_instituicao = Convert.ToInt32(cmbInstituicao.SelectedValue);
                funcionario.Cod_cargo = Convert.ToInt32(cmbCargo.SelectedValue);
                funcionario.Alterar();
                BtnClear_Click(sender, e);
            }
        }

        private void GrbResponsavel_Enter(object sender, EventArgs e)
        {

        }

        private void BtnExcluir_Click(object sender, EventArgs e)
        {

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbInstituicao_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    }

