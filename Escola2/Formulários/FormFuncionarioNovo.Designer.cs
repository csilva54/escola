﻿namespace Escola2.Formulários
{
    partial class FormFuncionarioNovo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbResponsavel = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbCargo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNascimento = new System.Windows.Forms.TextBox();
            this.Nivel = new System.Windows.Forms.Label();
            this.cmbNivel = new System.Windows.Forms.ComboBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.picbLogo = new System.Windows.Forms.PictureBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnListar = new System.Windows.Forms.Button();
            this.btnInserir = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCpf = new System.Windows.Forms.TextBox();
            this.txtRg = new System.Windows.Forms.TextBox();
            this.txtTelefone = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.cmbInstituicao = new System.Windows.Forms.ComboBox();
            this.btnPesq = new System.Windows.Forms.Button();
            this.txtPesq = new System.Windows.Forms.TextBox();
            this.grbListaResponsavel = new System.Windows.Forms.GroupBox();
            this.dtgResponsavel = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cpf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Instituição = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grbResponsavel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbLogo)).BeginInit();
            this.grbListaResponsavel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgResponsavel)).BeginInit();
            this.SuspendLayout();
            // 
            // grbResponsavel
            // 
            this.grbResponsavel.Controls.Add(this.label8);
            this.grbResponsavel.Controls.Add(this.cmbCargo);
            this.grbResponsavel.Controls.Add(this.label9);
            this.grbResponsavel.Controls.Add(this.txtNascimento);
            this.grbResponsavel.Controls.Add(this.Nivel);
            this.grbResponsavel.Controls.Add(this.cmbNivel);
            this.grbResponsavel.Controls.Add(this.btnClear);
            this.grbResponsavel.Controls.Add(this.picbLogo);
            this.grbResponsavel.Controls.Add(this.btnCancelar);
            this.grbResponsavel.Controls.Add(this.btnExcluir);
            this.grbResponsavel.Controls.Add(this.btnAlterar);
            this.grbResponsavel.Controls.Add(this.btnListar);
            this.grbResponsavel.Controls.Add(this.btnInserir);
            this.grbResponsavel.Controls.Add(this.label4);
            this.grbResponsavel.Controls.Add(this.label2);
            this.grbResponsavel.Controls.Add(this.label6);
            this.grbResponsavel.Controls.Add(this.label5);
            this.grbResponsavel.Controls.Add(this.label3);
            this.grbResponsavel.Controls.Add(this.label7);
            this.grbResponsavel.Controls.Add(this.label1);
            this.grbResponsavel.Controls.Add(this.txtCpf);
            this.grbResponsavel.Controls.Add(this.txtRg);
            this.grbResponsavel.Controls.Add(this.txtTelefone);
            this.grbResponsavel.Controls.Add(this.txtID);
            this.grbResponsavel.Controls.Add(this.txtNome);
            this.grbResponsavel.Controls.Add(this.cmbInstituicao);
            this.grbResponsavel.Location = new System.Drawing.Point(12, 12);
            this.grbResponsavel.Name = "grbResponsavel";
            this.grbResponsavel.Size = new System.Drawing.Size(680, 211);
            this.grbResponsavel.TabIndex = 1;
            this.grbResponsavel.TabStop = false;
            this.grbResponsavel.Text = "Funcionário";
            this.grbResponsavel.Enter += new System.EventHandler(this.GrbResponsavel_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(508, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Cargo";
            // 
            // cmbCargo
            // 
            this.cmbCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCargo.FormattingEnabled = true;
            this.cmbCargo.Location = new System.Drawing.Point(511, 91);
            this.cmbCargo.Name = "cmbCargo";
            this.cmbCargo.Size = new System.Drawing.Size(120, 21);
            this.cmbCargo.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(275, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Data de nascimento:";
            // 
            // txtNascimento
            // 
            this.txtNascimento.Location = new System.Drawing.Point(278, 41);
            this.txtNascimento.Name = "txtNascimento";
            this.txtNascimento.Size = new System.Drawing.Size(126, 20);
            this.txtNascimento.TabIndex = 8;
            // 
            // Nivel
            // 
            this.Nivel.AutoSize = true;
            this.Nivel.Location = new System.Drawing.Point(356, 76);
            this.Nivel.Name = "Nivel";
            this.Nivel.Size = new System.Drawing.Size(36, 13);
            this.Nivel.TabIndex = 7;
            this.Nivel.Text = "Nível ";
            this.Nivel.Click += new System.EventHandler(this.Label8_Click);
            // 
            // cmbNivel
            // 
            this.cmbNivel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNivel.FormattingEnabled = true;
            this.cmbNivel.Location = new System.Drawing.Point(359, 92);
            this.cmbNivel.Name = "cmbNivel";
            this.cmbNivel.Size = new System.Drawing.Size(137, 21);
            this.cmbNivel.TabIndex = 6;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(701, 19);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(30, 20);
            this.btnClear.TabIndex = 5;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // picbLogo
            // 
            this.picbLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picbLogo.Location = new System.Drawing.Point(782, 19);
            this.picbLogo.Name = "picbLogo";
            this.picbLogo.Size = new System.Drawing.Size(88, 87);
            this.picbLogo.TabIndex = 4;
            this.picbLogo.TabStop = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(790, 140);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(80, 40);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(267, 139);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(80, 40);
            this.btnExcluir.TabIndex = 3;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.BtnExcluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Location = new System.Drawing.Point(143, 139);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(80, 40);
            this.btnAlterar.TabIndex = 3;
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.BtnAlterar_Click);
            // 
            // btnListar
            // 
            this.btnListar.Location = new System.Drawing.Point(391, 139);
            this.btnListar.Name = "btnListar";
            this.btnListar.Size = new System.Drawing.Size(80, 40);
            this.btnListar.TabIndex = 3;
            this.btnListar.Text = "&Listar";
            this.btnListar.UseVisualStyleBackColor = true;
            this.btnListar.Click += new System.EventHandler(this.BtnListar_Click);
            // 
            // btnInserir
            // 
            this.btnInserir.Location = new System.Drawing.Point(19, 139);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(80, 40);
            this.btnInserir.TabIndex = 3;
            this.btnInserir.Text = "&Inserir";
            this.btnInserir.UseVisualStyleBackColor = true;
            this.btnInserir.Click += new System.EventHandler(this.BtnInserir_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(417, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Cpf:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(550, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Rg:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(161, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Instituição";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Telefone:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "ID";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(737, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Limpar";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome: ";
            // 
            // txtCpf
            // 
            this.txtCpf.Location = new System.Drawing.Point(420, 42);
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(117, 20);
            this.txtCpf.TabIndex = 1;
            // 
            // txtRg
            // 
            this.txtRg.Location = new System.Drawing.Point(553, 41);
            this.txtRg.Name = "txtRg";
            this.txtRg.Size = new System.Drawing.Size(119, 20);
            this.txtRg.TabIndex = 1;
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(16, 92);
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(126, 20);
            this.txtTelefone.TabIndex = 1;
            // 
            // txtID
            // 
            this.txtID.Enabled = false;
            this.txtID.Location = new System.Drawing.Point(16, 41);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(35, 20);
            this.txtID.TabIndex = 1;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(57, 41);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(208, 20);
            this.txtNome.TabIndex = 1;
            // 
            // cmbInstituicao
            // 
            this.cmbInstituicao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbInstituicao.FormattingEnabled = true;
            this.cmbInstituicao.Location = new System.Drawing.Point(164, 92);
            this.cmbInstituicao.Name = "cmbInstituicao";
            this.cmbInstituicao.Size = new System.Drawing.Size(177, 21);
            this.cmbInstituicao.TabIndex = 0;
            this.cmbInstituicao.SelectedIndexChanged += new System.EventHandler(this.cmbInstituicao_SelectedIndexChanged);
            // 
            // btnPesq
            // 
            this.btnPesq.Location = new System.Drawing.Point(574, 10);
            this.btnPesq.Name = "btnPesq";
            this.btnPesq.Size = new System.Drawing.Size(99, 22);
            this.btnPesq.TabIndex = 7;
            this.btnPesq.Text = "Pesquisar";
            this.btnPesq.UseVisualStyleBackColor = true;
            // 
            // txtPesq
            // 
            this.txtPesq.Location = new System.Drawing.Point(327, 241);
            this.txtPesq.Name = "txtPesq";
            this.txtPesq.Size = new System.Drawing.Size(253, 20);
            this.txtPesq.TabIndex = 6;
            // 
            // grbListaResponsavel
            // 
            this.grbListaResponsavel.Controls.Add(this.btnPesq);
            this.grbListaResponsavel.Controls.Add(this.dtgResponsavel);
            this.grbListaResponsavel.Location = new System.Drawing.Point(12, 229);
            this.grbListaResponsavel.Name = "grbListaResponsavel";
            this.grbListaResponsavel.Size = new System.Drawing.Size(680, 152);
            this.grbListaResponsavel.TabIndex = 5;
            this.grbListaResponsavel.TabStop = false;
            this.grbListaResponsavel.Text = "Lista de Funcionários";
            // 
            // dtgResponsavel
            // 
            this.dtgResponsavel.AllowUserToAddRows = false;
            this.dtgResponsavel.AllowUserToDeleteRows = false;
            this.dtgResponsavel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgResponsavel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Nome,
            this.Cpf,
            this.Instituição});
            this.dtgResponsavel.Location = new System.Drawing.Point(8, 35);
            this.dtgResponsavel.Name = "dtgResponsavel";
            this.dtgResponsavel.ReadOnly = true;
            this.dtgResponsavel.Size = new System.Drawing.Size(666, 111);
            this.dtgResponsavel.TabIndex = 1;
            this.dtgResponsavel.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgResponsavel_CellContentClick);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ID.Visible = false;
            this.ID.Width = 50;
            // 
            // Nome
            // 
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            this.Nome.Visible = false;
            this.Nome.Width = 170;
            // 
            // Cpf
            // 
            this.Cpf.HeaderText = "Cpf";
            this.Cpf.Name = "Cpf";
            this.Cpf.ReadOnly = true;
            this.Cpf.Visible = false;
            this.Cpf.Width = 170;
            // 
            // Instituição
            // 
            this.Instituição.HeaderText = "Instituição";
            this.Instituição.Name = "Instituição";
            this.Instituição.ReadOnly = true;
            this.Instituição.Visible = false;
            this.Instituição.Width = 207;
            // 
            // FormFuncionarioNovo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 375);
            this.Controls.Add(this.txtPesq);
            this.Controls.Add(this.grbListaResponsavel);
            this.Controls.Add(this.grbResponsavel);
            this.Name = "FormFuncionarioNovo";
            this.Text = "FormFuncionarioNovo";
            this.Load += new System.EventHandler(this.FormFuncionarioNovo_Load);
            this.grbResponsavel.ResumeLayout(false);
            this.grbResponsavel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbLogo)).EndInit();
            this.grbListaResponsavel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgResponsavel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbResponsavel;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.PictureBox picbLogo;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnListar;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCpf;
        private System.Windows.Forms.TextBox txtRg;
        private System.Windows.Forms.TextBox txtTelefone;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.ComboBox cmbInstituicao;
        private System.Windows.Forms.Button btnPesq;
        private System.Windows.Forms.TextBox txtPesq;
        private System.Windows.Forms.GroupBox grbListaResponsavel;
        private System.Windows.Forms.DataGridView dtgResponsavel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cpf;
        private System.Windows.Forms.DataGridViewTextBoxColumn Instituição;
        private System.Windows.Forms.Label Nivel;
        private System.Windows.Forms.ComboBox cmbNivel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNascimento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbCargo;
    }
}