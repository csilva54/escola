﻿namespace Escola2.Formulários
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTopo = new System.Windows.Forms.Panel();
            this.btnFechar = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.btnFuncionario = new System.Windows.Forms.Button();
            this.btnEvento = new System.Windows.Forms.Button();
            this.btnCrianca = new System.Windows.Forms.Button();
            this.btnResponsavel = new System.Windows.Forms.Button();
            this.btnInstituicao = new System.Windows.Forms.Button();
            this.panelConteudo = new System.Windows.Forms.Panel();
            this.btnHome = new System.Windows.Forms.Button();
            this.panelTopo.SuspendLayout();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTopo
            // 
            this.panelTopo.BackColor = System.Drawing.Color.Goldenrod;
            this.panelTopo.Controls.Add(this.btnFechar);
            this.panelTopo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTopo.Location = new System.Drawing.Point(0, 0);
            this.panelTopo.Name = "panelTopo";
            this.panelTopo.Size = new System.Drawing.Size(800, 37);
            this.panelTopo.TabIndex = 0;
            this.panelTopo.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTopo_Paint);
            // 
            // btnFechar
            // 
            this.btnFechar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFechar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnFechar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Location = new System.Drawing.Point(732, -3);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(65, 40);
            this.btnFechar.TabIndex = 0;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.panelMenu.Controls.Add(this.btnHome);
            this.panelMenu.Controls.Add(this.btnFuncionario);
            this.panelMenu.Controls.Add(this.btnEvento);
            this.panelMenu.Controls.Add(this.btnCrianca);
            this.panelMenu.Controls.Add(this.btnResponsavel);
            this.panelMenu.Controls.Add(this.btnInstituicao);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 37);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(88, 413);
            this.panelMenu.TabIndex = 1;
            // 
            // btnFuncionario
            // 
            this.btnFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.btnFuncionario.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFuncionario.FlatAppearance.BorderSize = 0;
            this.btnFuncionario.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnFuncionario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.btnFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuncionario.ForeColor = System.Drawing.Color.White;
            this.btnFuncionario.Location = new System.Drawing.Point(4, 306);
            this.btnFuncionario.Name = "btnFuncionario";
            this.btnFuncionario.Size = new System.Drawing.Size(81, 23);
            this.btnFuncionario.TabIndex = 2;
            this.btnFuncionario.Text = "Funcionario";
            this.btnFuncionario.UseVisualStyleBackColor = false;
            this.btnFuncionario.Click += new System.EventHandler(this.btnFuncionario_Click);
            // 
            // btnEvento
            // 
            this.btnEvento.BackColor = System.Drawing.Color.Transparent;
            this.btnEvento.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnEvento.FlatAppearance.BorderSize = 0;
            this.btnEvento.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnEvento.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.btnEvento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEvento.ForeColor = System.Drawing.Color.White;
            this.btnEvento.Location = new System.Drawing.Point(6, 113);
            this.btnEvento.Name = "btnEvento";
            this.btnEvento.Size = new System.Drawing.Size(75, 23);
            this.btnEvento.TabIndex = 1;
            this.btnEvento.Text = "Evento";
            this.btnEvento.UseVisualStyleBackColor = false;
            this.btnEvento.Click += new System.EventHandler(this.btnEvento_Click);
            // 
            // btnCrianca
            // 
            this.btnCrianca.BackColor = System.Drawing.Color.Transparent;
            this.btnCrianca.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnCrianca.FlatAppearance.BorderSize = 0;
            this.btnCrianca.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnCrianca.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.btnCrianca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCrianca.ForeColor = System.Drawing.Color.White;
            this.btnCrianca.Location = new System.Drawing.Point(7, 260);
            this.btnCrianca.Name = "btnCrianca";
            this.btnCrianca.Size = new System.Drawing.Size(75, 23);
            this.btnCrianca.TabIndex = 0;
            this.btnCrianca.Text = "Crianca";
            this.btnCrianca.UseVisualStyleBackColor = false;
            this.btnCrianca.Click += new System.EventHandler(this.btnCrianca_Click);
            // 
            // btnResponsavel
            // 
            this.btnResponsavel.BackColor = System.Drawing.Color.Transparent;
            this.btnResponsavel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnResponsavel.FlatAppearance.BorderSize = 0;
            this.btnResponsavel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnResponsavel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.btnResponsavel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResponsavel.ForeColor = System.Drawing.Color.White;
            this.btnResponsavel.Location = new System.Drawing.Point(3, 210);
            this.btnResponsavel.Name = "btnResponsavel";
            this.btnResponsavel.Size = new System.Drawing.Size(81, 23);
            this.btnResponsavel.TabIndex = 0;
            this.btnResponsavel.Text = "Responsavel";
            this.btnResponsavel.UseVisualStyleBackColor = false;
            this.btnResponsavel.Click += new System.EventHandler(this.btnResponsavel_Click);
            // 
            // btnInstituicao
            // 
            this.btnInstituicao.BackColor = System.Drawing.Color.Transparent;
            this.btnInstituicao.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnInstituicao.FlatAppearance.BorderSize = 0;
            this.btnInstituicao.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnInstituicao.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.btnInstituicao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInstituicao.ForeColor = System.Drawing.Color.White;
            this.btnInstituicao.Location = new System.Drawing.Point(7, 158);
            this.btnInstituicao.Name = "btnInstituicao";
            this.btnInstituicao.Size = new System.Drawing.Size(75, 23);
            this.btnInstituicao.TabIndex = 0;
            this.btnInstituicao.Text = "Instituicao";
            this.btnInstituicao.UseVisualStyleBackColor = false;
            this.btnInstituicao.Click += new System.EventHandler(this.btnInstituicao_Click);
            // 
            // panelConteudo
            // 
            this.panelConteudo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelConteudo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelConteudo.Location = new System.Drawing.Point(88, 37);
            this.panelConteudo.Name = "panelConteudo";
            this.panelConteudo.Size = new System.Drawing.Size(712, 413);
            this.panelConteudo.TabIndex = 2;
            this.panelConteudo.Paint += new System.Windows.Forms.PaintEventHandler(this.panelConteudo_Paint);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.Transparent;
            this.btnHome.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.ForeColor = System.Drawing.Color.White;
            this.btnHome.Location = new System.Drawing.Point(7, 63);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(75, 23);
            this.btnHome.TabIndex = 3;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panelConteudo);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panelTopo);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormEvento";
            this.panelTopo.ResumeLayout(false);
            this.panelMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTopo;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Panel panelConteudo;
        private System.Windows.Forms.Button btnCrianca;
        private System.Windows.Forms.Button btnResponsavel;
        private System.Windows.Forms.Button btnInstituicao;
        private System.Windows.Forms.Button btnFuncionario;
        private System.Windows.Forms.Button btnEvento;
        private System.Windows.Forms.Button btnHome;
    }
}