﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;


namespace Escola2
{
    public class Evento
    {
        private int id;
        private string nome;
        private string descricao;
        private DateTime dataCadastro;
        private DateTime dataEvento;
        private bool ativo;
        private int codigoInstituicao;
        Valida valida = new Valida();
        

        public int Id { get => id; set => id = value; }
        public string Descricao { get => descricao; set => descricao = value; }
        public DateTime DataCadastro { get => dataCadastro; set => dataCadastro = value; }
        public DateTime DataEvento { get => dataEvento; set => dataEvento = value; }
        public bool Ativo { get => ativo; set => ativo = value; }
        public int CodigoInstituicao { get => codigoInstituicao; set => codigoInstituicao = value; }
        public string Nome { get => nome; set => nome = value; }

        /// <summary>
        /// Esse é método construtor vazio da classe.
        /// </summary>
        public Evento()
        {

        }
        /// <summary>
        /// Este é o método com todos os parâmetros da classe. 
        /// </summary>
        public Evento(int _id, string _nome, string _descricao, DateTime _dataCadastro, DateTime _dataEvento, bool _ativo, int _codigoInstituicao)
        {
            this.Id = _id;
            this.Nome = _nome;
            this.Descricao = _descricao;
            this.DataCadastro = _dataCadastro;
            this.DataEvento = _dataEvento;
            this.Ativo = _ativo;
            this.CodigoInstituicao = _codigoInstituicao;
        }
        /// <summary>
        /// Este é o método com os parâmetros nome, descrição, data de cadastro ,data do evento, nivel de atividade e o codigo da instituição da classe. 
        /// </summary>
        public Evento(string _nome, string _descricao, DateTime _dataCadastro, DateTime _dataEvento, bool _ativo, int _codigoInstituicao)
        {
            this.Nome = _nome;
            this.Descricao = _descricao;
            this.DataCadastro = _dataCadastro;
            this.DataEvento = _dataEvento;
            this.Ativo = _ativo;
            this.CodigoInstituicao = _codigoInstituicao;

        }
        /// <summary>
        /// Este método serve para Inserir um Evento.Retorna uma váriavel booleana determinando se a operação foi executada ou não.
        /// </summary>
        /// <returns></returns>
        public bool Inserir()
        {
            bool validacao = false;
            try
            {
                var comando = Banco.Abrir();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "sp_eventos_inserir";
                comando.Parameters.Add("spnome", MySqlDbType.Text).Value = Nome;
                comando.Parameters.Add("spdescricao", MySqlDbType.Text).Value = Descricao;
                comando.Parameters.Add("spdata_cadastro", MySqlDbType.Timestamp).Value = DataCadastro;
                comando.Parameters.Add("spdata_evento", MySqlDbType.Timestamp).Value = DataEvento;
                comando.Parameters.Add("spativo", MySqlDbType.Bit).Value = Ativo;
                comando.Parameters.Add("spinstituicao", MySqlDbType.Int32).Value = CodigoInstituicao;
                this.id = Convert.ToInt32(comando.ExecuteScalar());
                Banco.Fechar(comando);
                validacao = true;

            }
            catch (Exception)
            {
                validacao = false;
            }

            return validacao;

        }
        /// <summary>
        ///  Este método altera o Evento especificado pelo Id.Retorna uma váriavel booleana determinando se a operação foi executada ou não.
        /// </summary>
        /// <returns></returns>
        public bool Alterar()
        {
            bool validacao = false; //variavel de validação da execução da alteração do Evento
            if (id > 0)
            {
                try //usando o try para a tentativa de execução para caso dê erro possamos tratar
                {
                    var comando = Banco.Abrir();
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandText = "sp_eventos_update";
                    comando.Parameters.Add("spid", MySqlDbType.Int32).Value = Id;
                    comando.Parameters.Add("spnome", MySqlDbType.Text).Value = Nome;
                    comando.Parameters.Add("spdescricao", MySqlDbType.Text).Value = Descricao;
                    comando.Parameters.Add("spdata_cadastro", MySqlDbType.Timestamp).Value = DataCadastro;
                    comando.Parameters.Add("spdata_evento", MySqlDbType.Timestamp).Value = DataEvento;
                    comando.Parameters.Add("spativo", MySqlDbType.Bit).Value = Ativo;
                    comando.Parameters.Add("spcod_instituicao", MySqlDbType.Int32).Value = CodigoInstituicao;
                    comando.ExecuteNonQuery();
                    Banco.Fechar(comando);
                    validacao = true; //retornando verdadeiro mostrando que a alteração foi feita corretamente
                }
                catch (Exception)
                {
                    validacao = false; //retornando falso mostrando que deu erro na alteração (possivelmente algum campo preenchido errado)
                }

            }

            return validacao;
        }
        /// <summary>
        /// Este método consulta todos os Eventos presentes no Banco.
        /// </summary>
        public MySqlDataReader ConsultaTodos()
        {
            var comando = Banco.Abrir();
            comando.CommandText = "select * from eventos";
            var dr = comando.ExecuteReader();
            Banco.Fechar(comando);
            return dr;
        }
        /// <summary>
        /// Este método necessita apenas do parâmetros Id para fazer uma consulta pelo Id do Evento.Retorna uma váriavel booleana determinando se a operação foi executada ou não.
        /// </summary>
        public bool ConsultarPorId(int _id)
        {
            bool validacao = false; //variavel de validação usada para determinar se a operação de consulta foi executada.
            try
            {
                var comando = Banco.Abrir();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "sp_eventos_select_id";
                comando.Parameters.Add("spid", MySqlDbType.Int32).Value = _id;
                var dr = comando.ExecuteReader();
                while (dr.Read())
                {
                    this.Id = dr.GetInt32(0);
                    this.Nome = dr.GetString(1);
                    this.Descricao = dr.GetString(2);
                    this.DataCadastro = dr.GetDateTime(3);
                    this.DataEvento = dr.GetDateTime(4);
                    this.Ativo = dr.GetBoolean(5);
                    this.CodigoInstituicao = dr.GetInt32(6);
                }
                Banco.Fechar(comando);
                validacao = true;
            }
            catch (Exception)
            {
                validacao = false;
            }
            return validacao;
        }

        public DataTable ListarGrid()
        {
            var comm = Banco.Abrir();
            string strComm = "select e.id as Codigo, e.Nome as Evento, e.data_evento as 'Data Evento', i.nome as Instituicao from eventos e join Instituicao i on i.Id = e.Cod_instituicao;";
            string strCon = @"server=127.0.0.1;user id=root;database=escola";
            MySqlConnection cn = new MySqlConnection(strCon);
            MySqlCommand objCommand = new MySqlCommand(strComm, cn);
            MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
            DataTable dt = new DataTable();
            objAdapter.Fill(dt);
            Banco.Fechar(comm);
            return dt;
        }

        public bool Deletar() {

            bool validacao = false;
            try //usando o try para a tentativa de execução para caso dê erro possamos tratar
            {
                var comando = Banco.Abrir();
                comando.CommandText = "delete from eventos where id = " + Id + "";
                var dr = comando.ExecuteReader();
                Banco.Fechar(comando);
                validacao = true;
            }
            catch (Exception)
            {
                validacao = false; //retornando falso mostrando que deu erro na alteração (possivelmente algum campo preenchido errado)
            }

            return validacao;
        }

        public DataTable Pesquisa(string quaisquer)
        {
            string strComm = "";
            var comm = Banco.Abrir();
            try
            {
                if (valida.ValidarNome(quaisquer))                
                strComm = "select e.id as Codigo, e.Nome as Evento, e.data_evento as 'Data Evento', i.nome as Instituicao from eventos e join Instituicao i on i.Id = e.Cod_instituicao where e.Descricao like '%" + quaisquer + "%';";
                string strCon = @"server=127.0.0.1;user id=root;database=escola";
                MySqlConnection cn = new MySqlConnection(strCon);
                MySqlCommand objCommand = new MySqlCommand(strComm, cn);
                MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
                DataTable dt = new DataTable();
                objAdapter.Fill(dt);
                return dt;
            }
            catch
            {
                if (valida.ValidarNome(quaisquer))
                strComm = "select e.id as Codigo, e.Nome as Evento, e.data_evento as 'Data Evento', i.nome as Instituicao from eventos e join Instituicao i on i.Id = e.Cod_instituicao where e.Descricao = 'limpo';";
                string strCon = @"server=127.0.0.1;user id=root;database=escola";
                MySqlConnection cn = new MySqlConnection(strCon);
                MySqlCommand objCommand = new MySqlCommand(strComm, cn);
                MySqlDataAdapter objAdapter = new MySqlDataAdapter(objCommand);
                DataTable dt = new DataTable();
                objAdapter.Fill(dt);
                return dt;
            };
        }

    }
}
