﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;


namespace Escola2
{
    public class AuxAdministrativo
    {
        private int cont;
        private int cont2;
        private int cont3;
        private int id;
        private string nome;
        private string rg;
        private string diaNascimento;
        private string mesNascimento;
        private string anoNascimento;
        private string nascimento;
        private string telefone;
        private string login;
        private string senha;
        private bool ativo;
        private int cod_nivel;
        private string cpf;
        private int cod_instituicao;

        public int Id { get => id; set => id = value; }
        public string Nome { get => nome; set => nome = value; }
        public string Rg { get => rg; set => rg = value; }
        public string Nascimento { get => nascimento; set => nascimento = value; }
        public string Telefone { get => telefone; set => telefone = value; }
        public string Login { get => login; set => login = value; }
        public string Senha { get => senha; set => senha = value; }
        public bool Ativo { get => ativo; set => ativo = value; }
        public int Cod_nivel { get => cod_nivel; set => cod_nivel = value; }
        public string Cpf { get => cpf; set => cpf = value; }
        public int Cod_instituicao { get => cod_instituicao; set => cod_instituicao = value; }
        public string DiaNascimento { get => diaNascimento; set => diaNascimento = value; }
        public string MesNascimento { get => mesNascimento; set => mesNascimento = value; }
        public string AnoNascimento { get => anoNascimento; set => anoNascimento = value; }

        public AuxAdministrativo() { }
        public AuxAdministrativo(int _id, string _cpf) { this.id = _id; this.cpf = _cpf; }
        public AuxAdministrativo(string _cpf) { this.cpf = _cpf; }
        public AuxAdministrativo(int _id, string _nome, string _rg, string _nascimento, string _telefone, string _login,
            string _senha, bool _ativo, int _cod_nivel, string _cpf, int _cod_instituicao)
        {
            this.id = _id;
            this.nome = _nome;
            this.rg = _rg;
            this.nascimento = _nascimento;
            this.telefone = _telefone;
            this.login = _login;
            this.senha = _senha;
            this.ativo = _ativo;
            this.cod_nivel = _cod_nivel;
            this.cpf = _cpf;
            this.cod_instituicao = _cod_instituicao;
        }
        public AuxAdministrativo(string _nome, string _rg, string _nascimento, string _telefone, string _login,
    string _senha, bool _ativo, int _cod_nivel, string _cpf, int _cod_instituicao)
        {
            this.nome = _nome;
            this.rg = _rg;
            this.nascimento = _nascimento;
            this.telefone = _telefone;
            this.login = _login;
            this.senha = _senha;
            this.ativo = _ativo;
            this.cod_nivel = _cod_nivel;
            this.cpf = _cpf;
            this.cod_instituicao = _cod_instituicao;
        }
        public AuxAdministrativo(int _id, string _nome, string _rg, string _nascimento, string _telefone, string _login,
    string _senha, string _cpf)
        {
            this.id = _id;
            this.nome = _nome;
            this.rg = _rg;
            this.nascimento = _nascimento;
            this.telefone = _telefone;
            this.login = _login;
            this.senha = _senha;
            this.cpf = _cpf;
        }
        public AuxAdministrativo(string _nome, string _rg, string _nascimento, string _telefone, string _login,
string _senha, string _cpf)
        {
            this.nome = _nome;
            this.rg = _rg;
            this.nascimento = _nascimento;
            this.telefone = _telefone;
            this.login = _login;
            this.senha = _senha;
            this.cpf = _cpf;
        }
        public void Inserir()
        {
            bool rgV = false;
            bool nomeV = false;
            bool telefoneV = false;
            bool cpfV = false;
            bool nascimentoV = false;
            try
            {
                var comm = Banco.Abrir();

                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.CommandText = "sp_aux_administrativo_inserir";

                if (rg != string.Empty && rg.Length <= 12)
                {
                    if (ValidarRG(rg))
                    {
                        rg = RgFormat(rg);
                        if (RgExistente(rg) == 0)
                        {
                            comm.Parameters.Add("sprg", MySqlDbType.VarChar).Value = rg;
                            rgV = true;
                        }
                    }
                }
                if (nome != string.Empty)
                {
                    if (ValidarNome(nome))
                    {
                        comm.Parameters.Add("spnome", MySqlDbType.VarChar).Value = nome;
                        nomeV = true;
                    }
                }
                nascimento = NascimentoFormat(diaNascimento, mesNascimento, anoNascimento);
                if (ValidarNascimento(nascimento))
                {
                    comm.Parameters.Add("spnascimento", MySqlDbType.VarChar).Value = nascimento;
                    nascimentoV = true;
                }
                if (telefone != string.Empty)
                {
                    if (ValidarTelefone(telefone))
                    {
                        telefone = TelefoneFormat(telefone);
                        if (TelefoneExiste(telefone) == 0)
                        {
                            comm.Parameters.Add("sptelefone", MySqlDbType.VarChar).Value = telefone;
                            telefoneV = true;
                        }
                    }
                }
                if (cpf != string.Empty)
                {
                    if (ValidarCpf(cpf))
                    {
                        cpf = CpfFormat(cpf);
                        if (CpfExistente(cpf) == 0)
                        {
                            comm.Parameters.Add("spcpf", MySqlDbType.VarChar).Value = cpf;
                            cpfV = true;
                        }
                    }
                }
                comm.Parameters.Add("splogin", MySqlDbType.VarChar).Value = login;
                comm.Parameters.Add("spsenha", MySqlDbType.VarChar).Value = senha;
                comm.Parameters.Add("spativo", MySqlDbType.Bit).Value = ativo;
                comm.Parameters.Add("spcod_nivel", MySqlDbType.Int32).Value = cod_nivel;
                if (cod_instituicao.ToString() != string.Empty)
                {
                    comm.Parameters.Add("spinstituicao", MySqlDbType.Int32).Value = cod_instituicao;
                }
                if (rgV &&
                     nomeV &&
                     telefoneV &&
                     cpfV && nascimentoV)
                {
                    MessageBox.Show("Aux administrador inserido com sucesso");
                }
                id = Convert.ToInt32(comm.ExecuteScalar());
                comm.Connection.Close();
            }
            catch
            {
                if(rgV == false && nomeV == false && telefoneV == false && cpfV == false && nascimentoV == false)
                    MessageBox.Show("Campos Inválidos");
                else if (rgV == false)
                    MessageBox.Show("Rg inválido");
                else if (nomeV == false)
                    MessageBox.Show("Nome inválido");
                else if (telefoneV == false)
                    MessageBox.Show("Telefone inválido");
                else if (cpfV == false)
                    MessageBox.Show("Cpf inválido");
                else if (nascimentoV == false)
                    MessageBox.Show("nascimento inválido");
            }
        }
        /// <summary>
        /// metodo para validação do rg
        /// </summary>
        /// <param name="_rg"></param>
        /// <returns></returns>
        private bool ValidarRG(string _rg)
        {
            var _rgV = RgFormat(_rg);
            if (_rgV.Length < 10)
            {
                int[] tamanho = new int[9];
                tamanho[0] = int.Parse(_rgV.Substring(0, 1)) * 2;
                tamanho[1] = int.Parse(_rgV.Substring(1, 1)) * 3;
                tamanho[2] = int.Parse(_rgV.Substring(2, 1)) * 4;
                tamanho[3] = int.Parse(_rgV.Substring(3, 1)) * 5;
                tamanho[4] = int.Parse(_rgV.Substring(4, 1)) * 6;
                tamanho[5] = int.Parse(_rgV.Substring(5, 1)) * 7;
                tamanho[6] = int.Parse(_rgV.Substring(6, 1)) * 8;
                tamanho[7] = int.Parse(_rgV.Substring(7, 1)) * 9;
                tamanho[8] = int.Parse(_rgV.Substring(8, 1)) * 100;
                var total = 0;
                for (int i = 0; i < 9; i++)
                {
                    total += tamanho[i];
                }
                var resto = 0;
                resto = total % 11;
                if (resto != 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// metodo que deixa todos rgs com um formato padrão
        /// </summary>
        /// <param name="_rg"></param>
        /// <returns></returns>
        private string RgFormat(string _rg)
        {
            int num = _rg.Length - 1;
            string _rgV = "";
            for (int i = 0; i <= num; i++)
            {
                if (!Regex.IsMatch(_rg.Substring(i, 1), @"^[0-9]+$"))
                {
                    _rg.Remove(i, 1);
                }
                else
                {
                    _rgV += _rg.Substring(i, 1);
                }
            }
            return _rgV;
        }
        /// <summary>
        /// metodo que identifica se o rg ja existe no banco
        /// </summary>
        /// <param name="_rg"></param>
        /// <returns></returns>
        private int RgExistente(string _rg)
        {

            var comando = Banco.Abrir();
            comando.CommandText = "select count(rg) from responsavel where rg = '" + _rg + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont = dr.GetInt32(0);
            }
            return cont;
        }
        /// <summary>
        /// valida se o nome é composto de carcteres normais
        /// </summary>
        /// <param name="_nome"></param>
        /// <returns></returns>
        private bool ValidarNome(string _nome)
        {
            if (!Regex.IsMatch(_nome, @"^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$"))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// formatação do cpf para padrão
        /// </summary>
        /// <param name="_cpf"></param>
        /// <returns></returns>
        private string CpfFormat(string _cpf)
        {
            int num = _cpf.Length - 1;
            string _cpfV = "";
            for (int i = 0; i <= num; i++)
            {
                if (!Regex.IsMatch(_cpf.Substring(i, 1), @"^[0-9]+$"))
                {
                    _cpf.Remove(i, 1);
                }
                else
                {
                    _cpfV += _cpf.Substring(i, 1);
                }
            }
            return _cpfV;
        }
        /// <summary>
        /// validação do cpf
        /// </summary>
        /// <param name="_cpf"></param>
        /// <returns></returns>
        private bool ValidarCpf(string _cpf)
        {
            if (_cpf.Length <= 14)
            {
                var _cpfV = CpfFormat(_cpf);
                int d1, d2;
                int soma = 0;
                string digitado = "";
                string calculado = "";

                // Pesos para calcular o primeiro digito
                int[] peso1 = new int[] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };

                // Pesos para calcular o segundo digito
                int[] peso2 = new int[] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
                int[] n = new int[11];
                for (int i = 0; i < 11; i++)
                {
                    n[i] = int.Parse(_cpfV.Substring(i, 1));
                }


                // Caso coloque todos os numeros iguais
                switch (_cpfV)
                {
                    case "11111111111":
                        return false;
                    case "00000000000":
                        return false;
                    case "2222222222":
                        return false;
                    case "33333333333":
                        return false;
                    case "44444444444":
                        return false;
                    case "55555555555":
                        return false;
                    case "66666666666":
                        return false;
                    case "77777777777":
                        return false;
                    case "88888888888":
                        return false;
                    case "99999999999":
                        return false;
                }


                // Calcula cada digito com seu respectivo peso
                for (int i = 0; i <= peso1.GetUpperBound(0); i++)
                    soma += (peso1[i] * Convert.ToInt32(n[i]));

                // Pega o resto da divisao
                int resto = soma % 11;

                if (resto == 1 || resto == 0)
                    d1 = 0;
                else
                    d1 = 11 - resto;

                soma = 0;

                // Calcula cada digito com seu respectivo peso
                for (int i = 0; i <= peso2.GetUpperBound(0); i++)
                    soma += (peso2[i] * Convert.ToInt32(n[i]));

                // Pega o resto da divisao
                resto = soma % 11;
                if (resto == 1 || resto == 0)
                    d2 = 0;
                else
                    d2 = 11 - resto;

                calculado = d1.ToString() + d2.ToString();
                digitado = n[9].ToString() + n[10].ToString();

                // Se os ultimos dois digitos calculados bater com
                // os dois ultimos digitos do cpf entao é válido
                if (calculado == digitado)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// metodo que confere se o cpf ja existe
        /// </summary>
        /// <param name="_cpf"></param>
        /// <returns></returns>
        private int CpfExistente(string _cpf)
        {
            var comando = Banco.Abrir();
            comando.CommandText = "select count(cpf) from responsavel where cpf = '" + _cpf + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont2 = dr.GetInt32(0);
            }
            return cont2;
        }
        /// <summary>
        /// metodo para padronizar o telefone
        /// </summary>
        /// <param name="_telefone"></param>
        /// <returns></returns>
        private string TelefoneFormat(string _telefone)
        {
            string telV = "";
            int num = _telefone.Length - 1;
            for (int i = 0; i <= num; i++)
            {
                if (!Regex.IsMatch(_telefone.Substring(i, 1), @"^[0-9]+$"))
                {
                    _telefone.Remove(i, 1);
                }
                else
                {
                    telV += _telefone.Substring(i, 1);
                }
            }

            return telV;
        }
        /// <summary>
        /// metodo para validar o telefone
        /// </summary>
        /// <param name="_telefone"></param>
        /// <returns></returns>
        private bool ValidarTelefone(string _telefone)
        {
            if (_telefone.Length <= 19)
            {
                _telefone = TelefoneFormat(_telefone);
                if (_telefone.Length == 9 || _telefone.Length == 11 || _telefone.Length == 13)
                {
                    for (int i = 0; i < _telefone.Length; i++)
                    {
                        if (Regex.IsMatch(_telefone.Substring(i, 1), @"^[0-9]+$"))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        /// <summary>
        /// metodo para ver se o telefone n existe
        /// </summary>
        /// <param name="_telefone"></param>
        /// <returns></returns>
        private int TelefoneExiste(string _telefone)
        {
            var comando = Banco.Abrir();
            comando.CommandText = "select count(telefone) from responsavel where telefone = '" + _telefone + "'";
            var dr = comando.ExecuteReader();
            while (dr.Read())
            {
                cont3 = dr.GetInt32(0);
            }
            return cont3;
        }
        private string NascimentoFormat(string _dia, string _mes, string _ano)
        {
            string nascimento = "";


            if (ValidarDia(_dia))
            {
                if (_dia.Length == 1)
                {
                    _dia.Insert(0, "0");
                    nascimento = _dia;
                }
                else
                    nascimento = _dia;
            }
            if (ValidarMes(_mes))
            {
                if (_mes.Length == 1)
                {
                    _mes.Insert(0, "0");
                    nascimento += _mes;
                }
                else
                {
                    nascimento += _mes;
                }
            }
            if (ValidarAno(_ano))
            {
                nascimento += _ano;
            }
            nascimento.Insert(2, "/");
            nascimento.Insert(5, "/");
            return nascimento;
        }
        /// <summary>
        /// metodo para validar o dia 
        /// </summary>
        /// <param name="_dia"></param>
        /// <returns></returns>
        private bool ValidarDia(string _dia)
        {
            if (_dia.Length <= 2 && _dia.Length != 0)
            {
                for (int i = 0; i < _dia.Length; i++)
                {
                    if (!Regex.IsMatch(_dia.Substring(i, 1), @"^[0-9]+$"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// metoedo para validar o mes
        /// </summary>
        /// <param name="_mes"></param>
        /// <returns></returns>
        private bool ValidarMes(string _mes)
        {
            if (_mes.Length <= 2 && _mes.Length != 0)
            {
                for (int i = 0; i < _mes.Length; i++)
                {
                    if (!Regex.IsMatch(_mes.Substring(i, 1), @"^[0-9]+$"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Metodo para validar o ano
        /// </summary>
        /// <param name="_ano"></param>
        /// <returns></returns>
        private bool ValidarAno(string _ano)
        {
            if (_ano.Length >= 2 && _ano.Length <= 4)
            {
                for (int i = 0; i < _ano.Length; i++)
                {
                    if (!Regex.IsMatch(_ano.Substring(i, 1), @"^[0-9]+$"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Metodo para validar a data de nascimento
        /// </summary>
        /// <param name="_nascimento"></param>
        /// <returns></returns>
        private bool ValidarNascimento(string _nascimento)
        {
            if (_nascimento.Length == 10 || _nascimento.Length == 8)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Alterar()
        {
            bool rgV = false;
            bool nomeV = false;
            bool telefoneV = false;
            bool cpfV = false;
            bool nascimentoV = false;
            try
            {
                var comm = Banco.Abrir();

                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.CommandText = "sp_aux_administrativo_update";

                if (rg != string.Empty && rg.Length <= 12)
                {
                    if (ValidarRG(rg))
                    {
                        rg = RgFormat(rg);
                        if (RgExistente(rg) == 0)
                        {
                            comm.Parameters.Add("sprg", MySqlDbType.VarChar).Value = rg;
                            rgV = true;
                        }
                    }
                }
                if (nome != string.Empty)
                {
                    if (ValidarNome(nome))
                    {
                        comm.Parameters.Add("spnome", MySqlDbType.VarChar).Value = nome;
                        nomeV = true;
                    }
                }
                nascimento = NascimentoFormat(diaNascimento, mesNascimento, anoNascimento);
                if (ValidarNascimento(nascimento))
                {
                    comm.Parameters.Add("spnascimento", MySqlDbType.VarChar).Value = nascimento;
                    nascimentoV = true;
                }
                if (telefone != string.Empty)
                {
                    if (ValidarTelefone(telefone))
                    {
                        telefone = TelefoneFormat(telefone);
                        if (TelefoneExiste(telefone) == 0)
                        {
                            comm.Parameters.Add("sptelefone", MySqlDbType.VarChar).Value = telefone;
                            telefoneV = true;
                        }
                    }
                }
                if (cpf != string.Empty)
                {
                    if (ValidarCpf(cpf))
                    {
                        cpf = CpfFormat(cpf);
                        if (CpfExistente(cpf) == 0)
                        {
                            comm.Parameters.Add("spcpf", MySqlDbType.VarChar).Value = cpf;
                            cpfV = true;
                        }
                    }
                }
                comm.Parameters.Add("splogin", MySqlDbType.VarChar).Value = login;
                comm.Parameters.Add("spsenha", MySqlDbType.VarChar).Value = senha;
                comm.Parameters.Add("spativo", MySqlDbType.Bit).Value = ativo;
                comm.Parameters.Add("spcod_nivel", MySqlDbType.Int32).Value = cod_nivel;
                if (cod_instituicao.ToString() != string.Empty)
                {
                    comm.Parameters.Add("spinstituicao", MySqlDbType.Int32).Value = cod_instituicao;
                }
                if (rgV &&
                     nomeV &&
                     telefoneV &&
                     cpfV && nascimentoV)
                {
                    MessageBox.Show("Aux administrador inserido com sucesso");
                }
                id = Convert.ToInt32(comm.ExecuteScalar());
                comm.Connection.Close();
            }
            catch
            {
                if (rgV == false)
                    MessageBox.Show("Rg inválido");
                else if (nomeV == false)
                    MessageBox.Show("Nome inválido");
                else if (telefoneV == false)
                    MessageBox.Show("Telefone inválido");
                else if (cpfV == false)
                    MessageBox.Show("Cpf inválido");
                else if (nascimentoV == false)
                    MessageBox.Show("nascimento inválido");
            }
        }
        public void ConsultarPorId(int _id) // declarar método
        { //implementar método
            var comm = Banco.Abrir();
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "sp_aux_administrativo_select_id";
            comm.Parameters.Add("spid", MySqlDbType.Int32).Value = _id;
            var dr = comm.ExecuteReader();
            while (dr.Read())
            {
                this.id = dr.GetInt32(0);
                this.nome = dr.GetString(1);
                this.rg = dr.GetString(2);
                this.nascimento = dr.GetString(3);
                this.telefone = dr.GetString(4);
                this.cpf = dr.GetString(9);
                this.cod_instituicao = dr.GetInt32(10);
            }
            comm.Connection.Close();
        }
        /// <summary>
        /// metodo para consultar todos responsaveis
        /// </summary>
        /// <returns></returns>
        public MySqlDataReader ConsultarTodos()
        {
            var comm = Banco.Abrir();
            comm.CommandText = "select * from aux_administrativo";
            var dr = comm.ExecuteReader();
            return dr;
        }
    }
}
