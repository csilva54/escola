﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Escola2
{
    public class Banco
    {
        public static MySqlCommand Abrir()
        {
            string strConn = @"server=127.0.0.1;user id=root;database=escola";//RegEx
            MySqlConnection cn = new MySqlConnection(strConn);
            cn.Open();
            MySqlCommand comm = new MySqlCommand();
            comm.Connection = cn;
            return comm;
        }

        /// <summary>
        /// Esse método fecha a conexão com o Banco de dados.
        /// </summary>
        /// <param name="comando"></param>
        public static void Fechar(MySqlCommand comando)
        {
            if (comando.Connection.State == ConnectionState.Open)
            {
                comando.Connection.Close();
            }
        }
    }

}
